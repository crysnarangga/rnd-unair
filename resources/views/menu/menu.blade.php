@extends('partials.index')
@section('title', 'Menu Setting')
@section('css_page')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.css" integrity="sha512-3pIirOrwegjM6erE5gPSwkUzO+3cTjpnV9lexlNZqvupR64iZBnOOTiiLPb9M36zpMScbmUNIcHUqKD47M719g==" crossorigin="anonymous" referrerpolicy="no-referrer" />
@endsection
@section('content')
<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
    <!--begin::Subheader-->
    <div class="subheader subheader-solid" id="kt_subheader">
        <div class="container-fluid " style="display: flex;justify-content: flex-end;">
            <span class="pull-right" id="date"></span>&nbsp;&nbsp;         
            <span class="pull-right" id="time"></span>
        </div>
        <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <!--begin::Info-->
            <div class="d-flex align-items-center flex-wrap mr-2" style="column-gap: 3px">
                <!--begin::Page Title-->
                <span class="text-muted font-weight-bold mr-4">
                    <i class="far fa-clipboard text-success"></i>
                </span>
                <!--end::Page Title-->
                <!--begin::Actions-->
                <div class="subheader-separator subheader-separator-ver mt-2 mb-2 mr-4 bg-gray-200"></div>
                <h5 class="text-dark font-weight-bold mt-2 mb-2 mr-5">Menu Setting</h5>					
                <!--end::Actions-->
            </div>
            <!--end::Info-->
            <!--begin::Toolbar-->
            <!--end::Toolbar-->
        </div>
    </div>
    <!--end::Subheader-->
    		<!--begin::Entry-->
		<div class="d-flex flex-column-fluid">
			<!--begin::Container-->
			<div class="container-fluid">
				<!--begin::Notice-->
                <!--end::Notice-->
                <!--begin::Card-->
                <div class="card card-custom gutter-b">
                    <div class="card-header flex-wrap py-3">
                        <div class="card-title">
                            <h3 class="card-label">Data Menu
                            <span class="d-block text-muted pt-2 font-size-sm"></span></h3>
                        </div>
                        <div class="card-toolbar">
                            <!--begin::Button-->
                            <a id="tombol-tambah" name="addMenu" class="btn btn-primary font-weight-bolder btn-sm" href="javascript:void(0)">
                                <span class="svg-icon svg-icon-md">
                                    <!--begin::Svg Icon | path:assets/media/svg/icons/Design/Flatten.svg-->
                                    <!--end::Svg Icon-->
                                </span>+ Tambah</a>
                            </a>
                            <!--end::Button-->
                        </div>
                    </div>
                    <div class="card-body">
                            <!--begin: Search Form-->
                            <!--begin::Search Form-->
                            <!--end::Search Form-->
                            <!--end: Search Form-->
                            <!--begin: Datatable-->
                            <table class="table table-striped data-table table-responsive" id="table_menu">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Menu Name</th>
                                        <th>Level</th>
                                        <th>Parent</th>
                                        <th>Menu Link</th>
                                        <th>Menu Icon</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                            </table>
                            <!--end: Datatable-->
                    </div>
                </div>
                <!--end::Card-->
			</div>
			<!--end::Container-->
		</div>
		<!--end::Entry-->
    </div>
    <!--end::Content-->

    <!--begin::Modal - Create App-->
		<div class="modal fade" id="tambah-edit-modal" tabindex="-1" aria-hidden="true">
			<!--begin::Modal dialog-->
			<div class="modal-dialog modal-dialog-centered mw-900px modal-dialog-scrollable">
				<!--begin::Modal content-->
				<div class="modal-content">
					<!--begin::Modal header-->
					<div class="modal-header">
						<!--begin::Modal title-->
						<h2 id="modal-judul"></h2>
						<!--end::Modal title-->
						<!--begin::Close-->
						<div class="btn btn-sm btn-icon btn-active-color-primary" data-bs-dismiss="modal">
							<!--begin::Svg Icon | path: icons/duotune/arrows/arr061.svg-->
							<span class="svg-icon svg-icon-1">
								<svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
									<rect opacity="0.5" x="6" y="17.3137" width="16" height="2" rx="1" transform="rotate(-45 6 17.3137)" fill="currentColor" />
									<rect x="7.41422" y="6" width="16" height="2" rx="1" transform="rotate(45 7.41422 6)" fill="currentColor" />
								</svg>
							</span>
							<!--end::Svg Icon-->
						</div>
						<!--end::Close-->
					</div>
					<!--end::Modal header-->
					<!--begin::Modal body-->
					<div class="modal-body py-lg-10 px-lg-10">
						<!--begin:Form-->
                        <form role="form" class="form" id="form-tambah-edit" name="form-tambah-edit" enctype="multipart/formdata" method="">
                            <div class="modal-body" style="height: 300px;">
                                <div class="mb-7 update">
                                    <input type="hidden" name="id" id="id">
                                    <div class="form-group row">
                                        <label class="col-lg-3 col-form-label">Nama Menu:</label>
                                        <div class="col-lg-9">
                                            <input type="text" class="form-control" id="menu_name" name="menu_name" placeholder="e.g: Dashboard" value="" required/>
                                            <span class="form-text text-muted">Masukan Nama Menu</span>
                                        </div>
                                    </div> <br>
                                    <div class="form-group row">
                                        <label class="col-lg-3 col-form-label">Link Menu:</label>
                                        <div class="col-lg-9">
                                            <input type="text" class="form-control" id="menu_link" name="menu_link" placeholder="e.g: dashboard" value="" required/>
                                            <span class="form-text text-muted">Masukan URL</span>
                                        </div>
                                    </div> <br>
                                    <div class="form-group row">
                                        <label class="col-lg-3 col-form-label">Ikon Menu:</label>
                                        <div class="col-lg-9">
                                            <input type="text" class="form-control" id="menu_icon" name="menu_icon" placeholder="e.g: fa fa-list"/>
                                            <span class="form-text text-muted">Masukan Ikon</span>
                                        </div>
                                    </div> <br>
                                    <div class="form-group row">
                                        <label class="col-lg-3 col-form-label">Level:</label>
                                        <div class="col-lg-9">
                                            <select class="form-control" id="id_level" name="id_level" required onchange="yesnoCheck(this)">
                                                <option value="">Pilih Level</option>
                                                @foreach($level as $l)
                                                    <option value="{{$l->id}}">{{$l->level}}</option>
                                                @endforeach
                                            </select>
                                            <span class="form-text text-muted">Pilih Level</span>
                                        </div>
                                    </div> <br>
                                    <div class="form-group row" id="ifYes" style="display: none;">
                                        <label class="col-lg-3 col-form-label">Parent:</label>
                                        <div class="col-lg-9">
                                            <select class="form-control" id="parent" name="parent" required>
                                                <option value="">Pilih Level</option>
                                                @foreach($menu as $l)
                                                    <option value="{{$l->id}}">{{ $l->id }} - {{$l->menu_name}}</option>
                                                @endforeach
                                            </select>
                                            <span class="form-text text-muted">Pilih Level</span>
                                        </div>
                                    </div> <br>
                                </div>
                            </div>
                            <script type="text/javascript">
                                function yesnoCheck(that) {
                                    if (that.value == "2") {
                                        document.getElementById("ifYes").style.display = "flex";
                                    } else {
                                        document.getElementById("ifYes").style.display = "none";
                                    }
                                }
                            </script>
                            <div class="modal-footer">
                                <button type="submit" id="tombol-simpan" value="create" class="btn btn-primary font-weight-bold">
                                    <i class="fa fa-save"></i> Save changes
                                </button>
                            </div>
                        </form>
                        <!--end:Form-->
					</div>
					<!--end::Modal body-->
				</div>
				<!--end::Modal content-->
			</div>
			<!--end::Modal dialog-->
		</div>
<!-- MULAI MODAL KONFIRMASI DELETE-->

<div class="modal fade" tabindex="-1" role="dialog" id="konfirmasi-modal" data-backdrop="false">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">PERHATIAN</h5>
                <div class="btn btn-sm btn-icon btn-active-color-primary" data-bs-dismiss="modal">
                    <!--begin::Svg Icon | path: icons/duotune/arrows/arr061.svg-->
                    <span class="svg-icon svg-icon-1">
                        <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <rect opacity="0.5" x="6" y="17.3137" width="16" height="2" rx="1" transform="rotate(-45 6 17.3137)" fill="currentColor" />
                            <rect x="7.41422" y="6" width="16" height="2" rx="1" transform="rotate(45 7.41422 6)" fill="currentColor" />
                        </svg>
                    </span>
                    <!--end::Svg Icon-->
                </div>
            </div>
            <div class="modal-body">
                <p><b>Jika menghapus Menu maka</b></p>
                <p>*data menu tersebut hilang selamanya, apakah anda yakin?</p>
            </div>
            <div class="modal-footer bg-whitesmoke br">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                <button type="button" class="btn btn-danger" name="tombol-hapus" id="tombol-hapus">Hapus
                    Data</button>
            </div>
        </div>
    </div>
</div>

<!-- AKHIR MODAL -->





@endsection
@section('js_page')
<!-- LIBARARY JS -->
<script type="text/javascript" language="javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>

<script type="text/javascript" language="javascript"
    src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.min.js"
    integrity="sha256-sPB0F50YUDK0otDnsfNHawYmA5M0pjjUf4TvRJkGFrI=" crossorigin="anonymous"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js" 
    integrity="sha512-VEd+nq25CkR676O+pLBnDW09R7VQX9Mdiij052gVCp5yVH3jGtH70Ho/UUv4mJDsEdTvqRCFZg0NKGiojGnUCw==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<!-- AKHIR LIBARARY JS -->

 <!-- JAVASCRIPT -->
 <script>
    //CSRF TOKEN PADA HEADER
    //Script ini wajib krn kita butuh csrf token setiap kali mengirim request post, patch, put dan delete ke server
    $(document).ready(function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    });

    //TOMBOL TAMBAH DATA
    //jika tombol-tambah diklik maka
    $('#tombol-tambah').click(function () {
        $('#button-simpan').val("create-post"); //valuenya menjadi create-post
        $('#id').val(''); //valuenya menjadi kosong
        $('#form-tambah-edit').trigger("reset"); //mereset semua input dll didalamnya
        $('#modal-judul').html("Tambah Menu"); //valuenya tambah pegawai baru
        $('#tambah-edit-modal').modal('show'); //modal tampil
    });
    

    //MULAI DATATABLE
    //script untuk memanggil data json dari server dan menampilkannya berupa datatable
    $(document).ready(function () {
        $('#table_menu').DataTable({
            processing: true,
            serverSide: true, //aktifkan server-side 
            ajax: {
                url: './menu/list', //url untuk request data
                type: 'GET'
            },
            columns: [
                {
                    data:'id',
                    name:'ID',
                },
                {
                    data: 'menu_name',
                    name: 'Menu Name',
                },
                {
                    data: 'level',
                    name: 'Level',
                },
                {
                    data: 'parent_name',
                    name: 'Parent',
                },
                {
                    data: 'menu_link',
                    name: 'Menu Link'
                },
                {
                    data: 'menu_icon',
                    name: 'Menu Icon',
                    render: function (data, type, row) {
                        return '<i class="'+data+'"></i>';
                    }
                },
                {
                    data: 'action',
                    name: 'Action',
                    orderable: false,
                    searchable: false
                },
            ],
            order: [
                [0, 'asc']
            ]
        });
    });

    //SIMPAN & UPDATE DATA DAN VALIDASI (SISI CLIENT)
    //jika id = form-tambah-edit panjangnya lebih dari 0 atau bisa dibilang terdapat data dalam form tersebut maka
    //jalankan jquery validator terhadap setiap inputan dll dan eksekusi script ajax untuk simpan data
    if ($("#form-tambah-edit").length > 0) {
        $("#form-tambah-edit").validate({
            submitHandler: function (form) {
                var actionType = $('#tombol-simpan').val();
                $('#tombol-simpan').html('Sending..');

                $.ajax({
                    data: $('#form-tambah-edit')
                        .serialize(), //function yang dipakai agar value pada form-control seperti input, textarea, select dll dapat digunakan pada URL query string ketika melakukan ajax request
                    url: "{{ route('menu.store') }}", //url simpan data
                    type: "POST", //karena simpan kita pakai method POST
                    dataType: 'json', //data tipe kita kirim berupa JSON
                    success: function (data) { //jika berhasil 
                        $('#form-tambah-edit').trigger("reset"); //form reset
                        $('#tambah-edit-modal').modal('hide'); //modal hide
                        $('#tombol-simpan').html('Simpan'); //tombol simpan
                        var oTable = $('#table_menu').dataTable(); //inialisasi datatable
                        oTable.fnDraw(false); //reset datatable
                        iziToast.success({ //tampilkan iziToast dengan notif data berhasil disimpan pada posisi kanan bawah
                            title: 'Data Berhasil Disimpan',
                            message: '{{ Session('
                            success ')}}',
                            position: 'bottomRight'
                        });
                        toastr.success('Data Berhasil Disimpan'); //tampilkan toastr dengan notif data berhasil disimpan pada posisi kiri bawah
                        window.location.reload();
                    },
                    error: function (data) { //jika error tampilkan error pada console
                        console.log('Error:', data);
                        toastr.error('Data Gagal Disimpan'); //tampilkan toastr dengan notif data gagal disimpan pada posisi kiri bawah
                        $('#tombol-simpan').html('Simpan');
                    }
                });
            }
        })
    }

    //TOMBOL EDIT DATA PER PEGAWAI DAN TAMPIKAN DATA BERDASARKAN ID PEGAWAI KE MODAL
    //ketika class edit-post yang ada pada tag body di klik maka
    $('body').on('click', '.edit-post', function () {
        var data_id = $(this).data('id');
        $.get('menu/' + data_id + '/edit', function (data) {
            $('#modal-judul').html("Edit");
            $('#tombol-simpan').val("edit-post");
            $('#tambah-edit-modal').modal('show');

            //set value masing-masing id berdasarkan data yg diperoleh dari ajax get request diatas               
            $('#id').val(data.id);
            $('#menu_name').val(data.menu_name);
            $('#id_level').val(data.id_level).trigger('change');
            $('#menu_link').val(data.menu_link);
            $('#menu_icon').val(data.menu_icon);
            $('#parent').val(data.parent_id).trigger('change');
            toastr.info('Data Berhasil Diambil'); //tampilkan toastr dengan notif data berhasil diedit pada posisi kiri bawah
            // menambah column input
        })
    });

    //jika klik class delete (yang ada pada tombol delete) maka tampilkan modal konfirmasi hapus maka
    $(document).on('click', '.delete', function () {
        dataId = $(this).attr('id');
        $('#konfirmasi-modal').modal('show');
    });

    $(document).on('click', '.submenu', function () {
        dataId = $(this).attr('id');
        $('#konfirmasi-submenu').modal('show');
        $('#modal-judul').html("Tambah Sub Menu"); //valuenya tambah pegawai baru
    });


    //jika tombol hapus pada modal konfirmasi di klik maka
    $('#tombol-hapus').click(function () {
        $.ajax({

            url: "menu/" + dataId, //eksekusi ajax ke url ini
            type: 'delete',
            beforeSend: function () {
                $('#tombol-hapus').text('Hapus Data'); //set text untuk tombol hapus
            },
            success: function (data) { //jika sukses
                setTimeout(function () {
                    $('#konfirmasi-modal').modal('hide'); //sembunyikan konfirmasi modal
                    var oTable = $('#table_menu').dataTable();
                    oTable.fnDraw(false); //reset datatable
                });
                iziToast.warning({ //tampilkan izitoast warning
                    title: 'Data Berhasil Dihapus',
                    message: '{{ Session('
                    delete ')}}',
                    position: 'bottomRight'
                });
                toastr.success('Data Berhasil Dihapus'); //tampilkan toastr dengan notif data berhasil dihapus pada posisi kiri bawah
            }
        })
    });
        if ($("#form-tambah-edit").length > 0) {
        $("#form-tambah-edit").validate({
            submitHandler: function (form) {
                var actionType = $('#tombol-simpan').val();
                $('#tombol-simpan').html('Sending..');

                $.ajax({
                    data: $('#form-tambah-edit')
                        .serialize(), //function yang dipakai agar value pada form-control seperti input, textarea, select dll dapat digunakan pada URL query string ketika melakukan ajax request
                    url: "{{ route('menu.store') }}", //url simpan data
                    type: "POST", //karena simpan kita pakai method POST
                    dataType: 'json', //data tipe kita kirim berupa JSON
                    success: function (data) { //jika berhasil 
                        $('#form-tambah-edit').trigger("reset"); //form reset
                        $('#tambah-edit-modal').modal('hide'); //modal hide
                        $('#tombol-simpan').html('Simpan'); //tombol simpan
                        var oTable = $('#table_menu').dataTable(); //inialisasi datatable
                        oTable.fnDraw(false); //reset datatable
                        iziToast.success({ //tampilkan iziToast dengan notif data berhasil disimpan pada posisi kanan bawah
                            title: 'Data Berhasil Disimpan',
                            message: '{{ Session('
                            success ')}}',
                            position: 'bottomRight'
                        });
                    },
                    error: function (data) { //jika error tampilkan error pada console
                        console.log('Error:', data);
                        $('#tombol-simpan').html('Simpan');
                        toastr.error('Data Gagal Disimpan'); //tampilkan toastr dengan notif data gagal disimpan pada posisi kiri bawah
                    }
                });
            }
        })
    }
</script>

<!-- JAVASCRIPT -->
@endsection