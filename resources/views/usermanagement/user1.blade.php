@extends('partials.index')
@section('title', 'User')
@section('css_page')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.css" integrity="sha512-3pIirOrwegjM6erE5gPSwkUzO+3cTjpnV9lexlNZqvupR64iZBnOOTiiLPb9M36zpMScbmUNIcHUqKD47M719g==" crossorigin="anonymous" referrerpolicy="no-referrer" />
@endsection
@section('content')
<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
    <!--begin::Subheader-->
    <div class="subheader subheader-solid" id="kt_subheader">
        <div class="container-fluid " style="display: flex;justify-content: flex-end;">
            <span class="pull-right" id="date"></span>&nbsp;&nbsp;         
            <span class="pull-right" id="time"></span>
        </div>
        <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <!--begin::Info-->
            <div class="d-flex align-items-center flex-wrap mr-2" style="column-gap: 3px">
                <!--begin::Page Title-->
                <span class="text-muted font-weight-bold mr-4">
                    <i class="fa-solid fa-user text-warning"></i>
                </span>
                <!--end::Page Title-->

                <!--begin::Actions-->
                <div class="subheader-separator subheader-separator-ver mt-2 mb-2 mr-4 bg-gray-200"></div>
                <h5 class="text-dark font-weight-bold mt-2 mb-2 mr-5">User</h5>					
                <!--end::Actions-->

            </div>
            <!--end::Info-->
            <!--begin::Toolbar-->
            <!--end::Toolbar-->
        </div>
    </div>
    <!--end::Subheader-->
    		<!--begin::Entry-->
		<div class="d-flex flex-column-fluid">
			<!--begin::Container-->
			<div class="container-fluid">
				<!--begin::Notice-->
                <!--end::Notice-->
                <!--begin::Card-->
                <div class="card card-custom gutter-b">
                    <div class="card-header flex-wrap py-3">
                        <div class="card-title">
                            <h3 class="card-label">Data User
                            <span class="d-block text-muted pt-2 font-size-sm"></span></h3>
                        </div>
                        <div class="card-toolbar">
                            <!--begin::Button-->
                            <a id="tombol-tambah" name="addMenu" class="btn btn-primary font-weight-bolder" href="javascript:void(0)">
                                <span class="svg-icon svg-icon-md">
                                    <!--begin::Svg Icon | path:assets/media/svg/icons/Design/Flatten.svg-->
                                    <!--end::Svg Icon-->
                                </span>+ Tambah</a>
                            </a>
                            <!--end::Button-->
                        </div>
                    </div>
                    <div class="card-body">

                            <!--begin: Search Form-->
                            <!--begin::Search Form-->
                            <div class="mb-7">
                                <div class="row align-items-center">
                                    <div class="col-lg-9 col-xl-8">
                                    </div>
                                </div>
                            </div>
                            <!--end::Search Form-->
                            <!--end: Search Form-->
                            <!--begin: Datatable-->
                            <table class="table table-striped responsive nowrap" id="table_menu" width="100%" style="overflow-x: auto; display: block;">
                                <thead>
                                    <tr>
                                        <th>FOTO USER</th>
                                        <th>NAMA USER</th>
                                        <th>USERNAME</th>
                                        <th>JENIS USER</th>
                                        <th>EMAIL</th>
                                        <th>NO HP</th>
                                        <th>WA</th>
                                        <th>PIN</th>
                                        <th>STATUS USER</th>
                                        <th>ACTION</th>
                                    </tr>
                                </thead>
                            </table>
                            <!--end: Datatable-->
                    </div>
                </div>
                <!--end::Card-->
			</div>
			<!--end::Container-->
		</div>
		<!--end::Entry-->
    </div>
    <!--end::Content-->

    <!--begin::Modal - Create App-->
		<div class="modal fade" id="tambah-edit-modal" tabindex="-1" aria-hidden="true">
			<!--begin::Modal dialog-->
			<div class="modal-dialog modal-dialog-centered mw-900px modal-dialog-scrollable">
				<!--begin::Modal content-->
				<div class="modal-content">
					<!--begin::Modal header-->
					<div class="modal-header" style="background-color: #153253">
						<!--begin::Modal title-->
						<h2 id="modal-judul" style="color: white"></h2>
						<!--end::Modal title-->
						<!--begin::Close-->
						<div class="btn btn-sm btn-icon btn-active-color-primary" data-bs-dismiss="modal">
							<!--begin::Svg Icon | path: icons/duotune/arrows/arr061.svg-->
							<span class="svg-icon svg-icon-1">
								<svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
									<rect opacity="0.5" x="6" y="17.3137" width="16" height="2" rx="1" transform="rotate(-45 6 17.3137)" fill="currentColor" />
									<rect x="7.41422" y="6" width="16" height="2" rx="1" transform="rotate(45 7.41422 6)" fill="currentColor" />
								</svg>
							</span>
							<!--end::Svg Icon-->
						</div>
						<!--end::Close-->
					</div>
					<!--end::Modal header-->
					<!--begin::Modal body-->
					<div class="modal-body py-lg-10 px-lg-10">
						<!--begin:Form-->
                        <form role="form" class="form" id="form-tambah-edit" name="form-tambah-edit" enctype="multipart/form-data" method="">
                            <div class="modal-body" style="height: 300px;">
                                <div class="mb-7">
                                    <input type="hidden" name="id" id="id">
                                    <input type="hidden" name="id_foto" id="id_foto">
                                    <div class="form-group row">
                                        <label class="col-lg-3 col-form-label">NAMA USER:</label>
                                        <div class="col-lg-9">
                                            <input type="text" class="form-control" id="nama_user" name="nama_user" placeholder="e.g: John Dypth" value="" required/>
                                            <span class="form-text text-muted">Please enter name user</span>
                                        </div>
                                    </div> <br>
                                    <div class="form-group row">
                                        <label class="col-lg-3 col-form-label">USERNAME:</label>
                                        <div class="col-lg-9">
                                            <input type="text" class="form-control" id="username" name="username" placeholder="e.g: john_dypth" value="" required/>
                                            <span class="form-text text-muted">Please enter username</span><br>
                                            <span id="error_username"></span>
                                        </div>
                                    </div> <br>
                                    <div class="form-group row">
                                        <label class="col-lg-3 col-form-label">EMAIL:</label>
                                        <div class="col-lg-9">
                                            <input type="text" class="form-control" id="email" name="email" placeholder="e.g: johndypth@gmail.com"/>
                                            <span class="form-text text-muted">Please enter email</span>
                                        </div>
                                    </div> <br>
                                    <div class="form-group row">
                                        <label class="col-lg-3 col-form-label">WA:</label>
                                        <div class="col-lg-9">
                                            <input type="text" class="form-control" id="wa" name="wa" placeholder="e.g: 08822211155"/>
                                            <span class="form-text text-muted">Please enter wa number</span>
                                        </div>
                                    </div> <br>
                                    <div class="form-group row">
                                        <label class="col-lg-3 col-form-label">PIN:</label>
                                        <div class="col-lg-9">
                                            <input type="text" class="form-control" id="pin" name="pin" placeholder="e.g: 252267"/>
                                            <span class="form-text text-muted">Please enter pin number</span>
                                        </div>
                                    </div> <br>
                                    <div class="form-group row">
                                        <label class="col-lg-3 col-form-label">STATUS USER:</label>
                                        <div class="col-lg-9">
                                            <select class="form-control" id="status" name="status" required>
                                                <option value="">Pilih Status User</option>
                                                <option value="1">Aktif</option>
                                                <option value="0">Tidak Aktif</option>
                                            </select>
                                            <span class="form-text text-muted">Please enter status user</span>
                                        </div>
                                    </div> <br>
                                    <div class="form-group row">
                                        <label class="col-lg-3 col-form-label">ID JENIS USER :</label>
                                        <div class="col-lg-9">
                                            <select class="form-control select2" id="userid" name="userid" style="width: 100%;" required>
                                                <option value="">Pilih Jenis</option>
                                                <?php foreach($jenisuser as $row){ ?>
                                                    <option value="<?php echo $row->id; ?>"><?php echo $row->jenis_user; ?></option>
                                                <?php } ?>
                                            </select>
                                            <span class="form-text text-muted">Please enter user id</span>
                                        </div>
                                    </div><br>
                                    <div class="form-group row">
                                        <label class="col-lg-3 col-form-label">NO HP:</label>
                                        <div class="col-lg-9">
                                            <input type="text" class="form-control" id="hp" name="hp" placeholder="e.g: 0882215551"/>
                                            <span class="form-text text-muted">Please Enter Your Phone Number</span>
                                        </div>
                                    </div><br>
                                    <div class="form-group row">
                                        <label class="col-lg-3 col-form-label">USER FOTO:</label>
                                        <div class="col-lg-9">
                                            <input type="file" id="photo" class="form-control" name="photo">
                                            <span class="form-text text-muted">Please upload your photo</span>
                                        </div>
                                    </div> <br>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="submit" id="tombol-simpan" value="create" class="btn btn-primary font-weight-bold">
                                    <i class="fa fa-save"></i> Save changes
                                </button>
                            </div>
                        </form>
                        <!--end:Form-->
					</div>
					<!--end::Modal body-->
				</div>
				<!--end::Modal content-->
			</div>
			<!--end::Modal dialog-->
		</div>

<!-- MULAI MODAL KONFIRMASI DELETE-->

<div class="modal fade" tabindex="-1" role="dialog" id="konfirmasi-modal" data-backdrop="false">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">PERHATIAN</h5>
                <div class="btn btn-sm btn-icon btn-active-color-primary" data-bs-dismiss="modal">
                    <!--begin::Svg Icon | path: icons/duotune/arrows/arr061.svg-->
                    <span class="svg-icon svg-icon-1">
                        <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <rect opacity="0.5" x="6" y="17.3137" width="16" height="2" rx="1" transform="rotate(-45 6 17.3137)" fill="currentColor" />
                            <rect x="7.41422" y="6" width="16" height="2" rx="1" transform="rotate(45 7.41422 6)" fill="currentColor" />
                        </svg>
                    </span>
                    <!--end::Svg Icon-->
                </div>
            </div>
            <div class="modal-body">
                <p><b>Jika menghapus User maka</b></p>
                <p>*data user tersebut hilang selamanya, apakah anda yakin?</p>
            </div>
            <div class="modal-footer bg-whitesmoke br">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                <button type="button" class="btn btn-danger" name="tombol-hapus" id="tombol-hapus">Hapus
                    Data</button>
            </div>
        </div>
    </div>
</div>

<!-- Mulai modal menu --->
<div class="modal fade" id="menu-modal" tabindex="-1" aria-hidden="true">
    <!--begin::Modal dialog-->
    <div class="modal-dialog modal-dialog-centered mw-500px modal-dialog-scrollable">
        <!--begin::Modal content-->
        <div class="modal-content">
            <!--begin::Modal header-->
            <div class="modal-header" style="background-color: #153253">
                <!--begin::Modal title-->
                <h2 id="modal-judul" style="color: white">Setting Menu User</h2>
                <!--end::Modal title-->
                <!--begin::Close-->
                <div class="btn btn-sm btn-icon btn-active-color-primary" data-bs-dismiss="modal">
                    <!--begin::Svg Icon | path: icons/duotune/arrows/arr061.svg-->
                    <span class="svg-icon svg-icon-1">
                        <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <rect opacity="0.5" x="6" y="17.3137" width="16" height="2" rx="1" transform="rotate(-45 6 17.3137)" fill="currentColor" />
                            <rect x="7.41422" y="6" width="16" height="2" rx="1" transform="rotate(45 7.41422 6)" fill="currentColor" />
                        </svg>
                    </span>
                    <!--end::Svg Icon-->
                </div>
                <!--end::Close-->
            </div>
            <!--end::Modal header-->
            <!--begin::Modal body-->
            <div class="modal-body py-lg-10 px-lg-10">
                <!--begin:Form-->
                <form role="form" class="form" id="form-tambah-menu" name="form-tambah-edit" enctype="multipart/form-data" method="">
                    <div class="modal-body" style="height: 300px;">
                        <input type="hidden" name="iduser" id="iduser" value="">
                        @php
                            foreach ($menu as $key => $value) {
                                $count = 0;
                                if ($value->id_level == '1') {
                                    echo '<span class="text-primary h6">'.Str::upper($value->menu_name).'</span>';
                                    echo '<input type="checkbox" class="custom-control-input ok'.$value->id.'" id="'.$value->id.'" name="menu[]" value="'.$value->id.'" style="display:hidden; height: 0; width: 0">';
                                    foreach ($menu as $key1 => $value1) {
                                        if ($value1->parent_id == $value->id) {
                                            echo '<div class="form-group row">';
                                                echo '<div class="col-lg-5">';
                                                    echo '<i class="fa-solid fa-caret-right text-primary">&nbsp;</i>';
                                                    echo '<label class="col-form-label">&nbsp;&nbsp;&nbsp;'.$value1->menu_name.'</label>';
                                                echo '</div>';
                                                echo '<div class="col-lg-7">';
                                                    echo '<div class="custom-control custom-checkbox p-4">';
                                                        echo '<input type="checkbox" class="custom-control-input ok'.$value1->id.'" id="'.$value1->id.'" name="menu[]" value="'.$value1->id.'" onclick="myFunction()">';
                                                        echo '<label class="custom-control-label" for="'.$value1->id.'"></label>';
                                                    echo '</div>';
                                                echo '</div>';
                                            echo '</div>';
                                            $count++;
                                            echo "<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.1/jquery.min.js' integrity='sha512-aVKKRRi/Q/YV+4mjoKBsE4x3H+BkegoM/em46NNlCqNTmUYADjBbeNefNxYV7giUp0VxICtqdrbqU7iVaeZNXA==' crossorigin='anonymous' referrerpolicy='no-referrer'></script>";
                                            echo "<script>
                                                var chk1 = $('.ok".$value1->id."');
                                                var chk2 = $('.ok".$value->id."');

                                                chk1.on('click', function(){
                                                if( chk1.is(':checked') ) {
                                                    chk2.attr('checked', true);
                                                } else {
                                                    chk2.attr('checked', false);
                                                }
                                                });

                                                // var checkbox1 = document.getElementsByClassName('ok".$value1->id."');
                                                // var checkbox2 = document.getElementsByClassName('ok".$value->id."');
                                                // function myFunction(){
                                                //     var isChecked = checkbox1.checked;
                                                //     if(isChecked){
                                                //         checkbox2.checked = true;
                                                //     }
                                                // }
                                            </script>";
                                        }
                                    }
                                    if ($count == 0) {
                                        echo '<input type="checkbox" class="custom-control-input ok'.$value->id.'" id="'.$value->id.'" name="menu[]" value="'.$value->id.'" style="display:inline-block; margin-left: 83px;">';
                                        echo '<label class="custom-control-label" for="'.$value->id.'"></label>';
                                        echo '<br>';
                                    }
                                }
                            }
                        @endphp
                    </div>
                    <div class="modal-footer">
                        <button type="submit" id="tombol-simpan-menu" value="create" class="btn btn-primary font-weight-bold">
                            <i class="fa fa-save"></i> Save changes
                        </button>
                    </div>
                </form>
                <!--end:Form-->
            </div>
            <!--end::Modal body-->
        </div>
        <!--end::Modal content-->
    </div>
    <!--end::Modal dialog-->
</div>
<!-- Akhir modal menu -->

<!-- Mulai modal change password --->
<div class="modal fade" id="change-password-modal" tabindex="-1" aria-hidden="true">
    <!--begin::Modal dialog-->
    <div class="modal-dialog modal-dialog-centered mw-500px modal-dialog-scrollable">
        <!--begin::Modal content-->
        <div class="modal-content">
            <!--begin::Modal header-->
            <div class="modal-header" style="background-color: #153253">
                <!--begin::Modal title-->
                <h2 id="modal-judul" style="color: white">Change Password</h2>
                <!--end::Modal title-->
                <!--begin::Close-->
                <div class="btn btn-sm btn-icon btn-active-color-primary" data-bs-dismiss="modal">
                    <!--begin::Svg Icon | path: icons/duotune/arrows/arr061.svg-->
                    <span class="svg-icon svg-icon-1">
                        <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <rect opacity="0.5" x="6" y="17.3137" width="16" height="2" rx="1" transform="rotate(-45 6 17.3137)" fill="currentColor" />
                            <rect x="7.41422" y="6" width="16" height="2" rx="1" transform="rotate(45 7.41422 6)" fill="currentColor" />
                        </svg>
                    </span>
                    <!--end::Svg Icon-->
                </div>
                <!--end::Close-->
            </div>
            <!--end::Modal header-->
            <!--begin::Modal body-->
            <div class="modal-body py-lg-10 px-lg-10">
                <!--begin:Form-->
                <form role="form" class="form" id="form-change-password" name="form-change-password" enctype="multipart/form-data" method="">
                    <div class="modal-body" style="height: 200px;">
                        <input type="hidden" name="iduser_password" id="iduser_password" value="">
                        <div class="form-group row">
                            <div class="col-lg-12">
                                <label class="col-form-label">Password Baru</label>
                                <input type="password" class="form-control" id="password_baru" name="password_baru" placeholder="Password Baru">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-lg-12">
                                <label class="col-form-label">Konfirmasi Password</label>
                                <input type="password" class="form-control" id="konfirmasi_password" name="konfirmasi_password" placeholder="Konfirmasi Password">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" id="tombol-simpan-password" value="create" class="btn btn-primary font-weight-bold">
                            <i class="fa fa-save"></i> Save changes
                        </button>
                    </div>
                </form>
                <!--end:Form-->
            </div>
            <!--end::Modal body-->
        </div>
        <!--end::Modal content-->
    </div>
</div>

@endsection
@section('js_page')
<!-- LIBARARY JS -->
<script type="text/javascript" language="javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>


<script type="text/javascript" language="javascript"
    src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>


<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.min.js"
    integrity="sha256-sPB0F50YUDK0otDnsfNHawYmA5M0pjjUf4TvRJkGFrI=" crossorigin="anonymous"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js" 
    integrity="sha512-VEd+nq25CkR676O+pLBnDW09R7VQX9Mdiij052gVCp5yVH3jGtH70Ho/UUv4mJDsEdTvqRCFZg0NKGiojGnUCw==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    

<!-- AKHIR LIBARARY JS -->

 <!-- JAVASCRIPT -->
 <script>
    //CSRF TOKEN PADA HEADER
    //Script ini wajib krn kita butuh csrf token setiap kali mengirim request post, patch, put dan delete ke server
    $(document).ready(function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $('#username').blur(function() {
            var error_username = '';
            var username = $('#username').val();
            var _token = $('input[name="_token"]').val();
            $.ajax({
                url:'./user/check',
                method:"POST",
                data:{username:username, _token:_token},
                success:function(result) {
                    if(result == 'unique') {
                        $('#error_username').html('<label class="text-success"><i class="fa-solid fa-circle-check text-success"></i>&nbsp;Username available</label>');
                        $('#username').removeClass('has-error');
                        $('#tombol-simpan').attr('disabled', false);
                    }
                    else {
                        $('#error_username').html('<label class="text-danger"><i class="fa-solid fa-circle-xmark text-danger"></i>&nbsp;Username already exists</label>');
                        $('#username').addClass('has-error');
                        $('#tombol-simpan').attr('disabled', 'disabled');
                    }
                }
            })
        });
    });

    //TOMBOL TAMBAH DATA
    //jika tombol-tambah diklik maka
    $('#tombol-tambah').click(function () {
        $('#button-simpan').val("create-post"); //valuenya menjadi create-post
        $('#id').val(''); //valuenya menjadi kosong
        $('#form-tambah-edit').trigger("reset"); //mereset semua input dll didalamnya
        $('#modal-judul').html("Tambah User"); //valuenya tambah pegawai baru
        $('#tambah-edit-modal').modal('show'); //modal tampil
    });

    //MULAI DATATABLE
    //script untuk memanggil data json dari server dan menampilkannya berupa datatable
    $(document).ready(function () {
        $('#table_menu').DataTable({
            processing: true,
            serverSide: true, //aktifkan server-side 
            ajax: {
                url: './user/list', //url untuk request data
                type: 'GET'
            },
            columns: [
                {
                    data: 'no_dokumen',
                    name: 'FOTO_USER',
                    render: function (data) {
                            return '<img src="assets/media/img/profile/'+data+'" style="width:50px;height:50px;">';
                    }
                },
                {
                    data: 'nama_user',
                    name: 'NAMA USER'
                },
                {
                    data: 'username',
                    name: 'USERNAME'
                },
                {
                    data: 'jenis_user',
                    name: 'JENIS USER',
                    render : function (data) {
                        return '<span class="badge badge-primary">'+data+'</span>';
                    }
                },
                {
                    data: 'email',
                    name: 'EMAIL'
                },
                {
                    data: 'no_hp',
                    name: 'NO HP'
                },
                {
                    data: 'wa',
                    name: 'WA'
                },
                {
                    data: 'pin',
                    name: 'PIN'
                },
                {
                    data: 'status_user',
                    name: 'STATUS USER',
                    render : function (data) {
                        if (data == '1') {
                            return '<span class="badge badge-success">Aktif</span>';
                        } else {
                            return '<span class="badge badge-danger">Tidak Aktif</span>';
                        }
                    }
                },
                {
                    data: 'action',
                    name: 'Action',
                    orderable: false,
                    searchable: false
                },
            ],
            order: [
                [0, 'asc']
            ]
        });
    });

    //SIMPAN & UPDATE DATA DAN VALIDASI (SISI CLIENT)
    //jika id = form-tambah-edit panjangnya lebih dari 0 atau bisa dibilang terdapat data dalam form tersebut maka
    //jalankan jquery validator terhadap setiap inputan dll dan eksekusi script ajax untuk simpan data
    if ($("#form-tambah-edit").length > 0) {
        $("#form-tambah-edit").validate({
            submitHandler: function (form) {
                event.preventDefault();
                var actionType = $('#tombol-simpan').val();
                $('#tombol-simpan').html('Sending..');
                var form = $('#form-tambah-edit')[0];
                var formData = new FormData(form);
                var id = $('#id').val();

                if (id != '') {
                    $.ajax({
                        type: 'POST',
                        url: './user/update/' + id,
                        data: formData,
                        contentType: false,
                        processData: false,
                        success: function (data) {
                            $('#tombol-simpan').html('<i class="fa fa-save"></i> Save changes');
                            $('#tambah-edit-modal').modal('hide');
                            $('#table_menu').DataTable().ajax.reload();
                            $('#form-tambah-edit').trigger("reset");
                            toastr.success('Data Berhasil Diupdate', 'Success Alert', {
                                timeOut: 5000
                            });
                        },
                        error: function (data) {
                            $('#tombol-simpan').html('<i class="fa fa-save"></i> Save changes');
                            toastr.error('Terjadi kesalahan!');
                        }
                    });
                } else {
                    $.ajax({
                        type: 'POST',
                        url: './user/store',
                        data: formData,
                        contentType: false,
                        processData: false,
                        success: function (data) {
                            $('#tombol-simpan').html('<i class="fa fa-save"></i> Save changes');
                            $('#tambah-edit-modal').modal('hide');
                            $('#table_menu').DataTable().ajax.reload();
                            $('#form-tambah-edit').trigger("reset");
                            toastr.success('Data Berhasil Disimpan', 'Success Alert', {
                                timeOut: 5000
                            });
                        },
                        error: function (data) {
                            $('#tombol-simpan').html('<i class="fa fa-save"></i> Save changes');
                            toastr.error('Terjadi kesalahan!');
                        }
                    });
                }
            }
        });
    }



    //TOMBOL EDIT DATA PER PEGAWAI DAN TAMPIKAN DATA BERDASARKAN ID PEGAWAI KE MODAL
    //ketika class edit-post yang ada pada tag body di klik maka
    $('body').on('click', '.edit-post', function () {
        var data_id = $(this).data('id');
        $.get('user/' + data_id + '/edit', function (data) {
            $('#modal-judul').html("Edit");
            $('#tombol-simpan').val("edit-post");
            $('#tambah-edit-modal').modal('show');

            //set value masing-masing id berdasarkan data yg diperoleh dari ajax get request diatas               
            $('#id').val(data.user_id);
            $('#id_foto').val(data.user_foto);
            $('#nama_user').val(data.nama_user);
            $('#username').val(data.username);
            $('#email').val(data.email);
            $('#hp').val(data.no_hp);
            $('#wa').val(data.wa);
            $('#pin').val(data.pin);
            $('#status').val(data.status_user);
            $('#userid').val(data.id_jenis_user).trigger('change');
            toastr.info('Data Berhasil Diambil'); //tampilkan toastr dengan notif data berhasil diedit pada posisi kiri bawah

        })
    });

    $('body').on('click', '.password-post', function () {
        var data_id = $(this).data('id');
        $('#modal-judul').html("Edit Password");
        $('#tombol-simpan').val("edit-password");
        $('#change-password-modal').modal('show');

        //set value masing-masing id berdasarkan data yg diperoleh dari ajax get request diatas               
        $('#iduser_password').val(data_id);
    });

    if ($("#form-change-password").length > 0) {
        $("#form-change-password").validate({
            submitHandler: function (form) {
                event.preventDefault();
                var actionType = $('#tombol-simpan-password').val();
                $('#tombol-simpan-password').html('Sending..');
                var form = $('#form-change-password')[0];
                var formData = new FormData(form);

                var id = $('#id').val();
                $.ajax({
                    data: formData, //function yang dipakai agar value pada form-control seperti input, textarea, select dll dapat digunakan pada URL query string ketika melakukan ajax request
                    url: './user/changepassword/{id}', //url simpan data
                    type: "POST", //karena simpan kita pakai method POST
                    dataType: 'json', //data tipe kita kirim berupa JSON
                    processData: false,
                    contentType: false,
                    success: function (data) { //jika berhasil 
                        $('#form-change-password').trigger("reset"); //form reset
                        //tandai
                        $('#change-password-modal').modal('hide'); //modal hide
                        $('#tombol-simpan-password').html('Simpan'); //tombol simpan
                        var oTable = $('#table_menu').dataTable(); //inialisasi datatable
                        oTable.fnDraw(false); //reset datatable
                        toastr.success('Data Berhasil Disimpan'); //tampilkan toastr dengan notif data berhasil disimpan pada posisi kiri bawah
                    },
                    error: function (data) { //jika error tampilkan error pada console
                        console.log('Error:', data);
                        $('#tombol-simpan-menu').html('Simpan');
                        toastr.error('Data Gagal Disimpan'); //tampilkan toastr dengan notif data gagal disimpan pada posisi kiri bawah
                    }
                });
            }
        })
    }

    //jika klik class delete (yang ada pada tombol delete) maka tampilkan modal konfirmasi hapus maka
    $(document).on('click', '.delete', function () {
        dataId = $(this).attr('id');
        $('#konfirmasi-modal').modal('show');
    });

    //jika tombol hapus pada modal konfirmasi di klik maka
    $('#tombol-hapus').click(function () {
        event.preventDefault();
        $.ajax({
            url: "user/" + dataId, //eksekusi ajax ke url ini
            type: 'delete',
            beforeSend: function () {
                $('#tombol-hapus').text('Hapus Data'); //set text untuk tombol hapus
            },
            success: function (data) { //jika sukses
                setTimeout(function () {
                    $('#konfirmasi-modal').modal('hide'); //sembunyikan konfirmasi modal
                    var oTable = $('#table_menu').dataTable();
                    oTable.fnDraw(false); //reset datatable
                });
                iziToast.warning({ //tampilkan izitoast warning
                    title: 'Data Berhasil Dihapus',
                    message: '{{ Session('
                    delete ')}}',
                    position: 'bottomRight'
                });
                toastr.success('Data Berhasil Dihapus'); //tampilkan toastr dengan notif data berhasil dihapus pada posisi kiri bawah
            }
        })
    });

    $('body').on('click', '.menu-post', function () {
        var data_id = $(this).data('id');
        $('#form-tambah-menu').trigger("reset"); //form reset
        $.get('user/' + data_id + '/listmenu', function (data) {
            $('#modal-judul').html("Setting Menu");
            $('#menu-modal').modal('show');
            $.each(data, function (i, item) {
                $('.ok' + item.menu_id).prop('checked', true);
            });
            $('#iduser').val(data_id);
            toastr.info('Data Berhasil Diambil'); //tampilkan toastr dengan notif data berhasil diedit pada posisi kiri bawah
        })
    });

    if ($("#form-tambah-menu").length > 0) {
        $("#form-tambah-menu").validate({
            submitHandler: function (form) {
                event.preventDefault();
                var actionType = $('#tombol-simpan-menu').val();
                $('#tombol-simpan').html('Sending..');
                var form = $('#form-tambah-menu')[0];
                var formData = new FormData(form);

                $.ajax({
                    data: formData, //function yang dipakai agar value pada form-control seperti input, textarea, select dll dapat digunakan pada URL query string ketika melakukan ajax request
                    url: './user/storemenu', //url simpan data
                    type: "POST", //karena simpan kita pakai method POST
                    dataType: 'json', //data tipe kita kirim berupa JSON
                    processData: false,
                    contentType: false,
                    success: function (data) { //jika berhasil 
                        $('#form-tambah-menu').trigger("reset"); //form reset
                        $('#menu-modal').modal('hide'); //modal hide
                        $('#tombol-simpan-menu').html('Simpan'); //tombol simpan
                        var oTable = $('#table_menu').dataTable(); //inialisasi datatable
                        oTable.fnDraw(false); //reset datatable
                        iziToast.success({ //tampilkan iziToast dengan notif data berhasil disimpan pada posisi kanan bawah
                            title: 'Data Berhasil Disimpan',
                            message: '{{ Session('
                            success ')}}',
                            position: 'bottomRight'
                        });
                        toastr.success('Data Berhasil Disimpan'); //tampilkan toastr dengan notif data berhasil disimpan pada posisi kiri bawah
                    },
                    error: function (data) { //jika error tampilkan error pada console
                        console.log('Error:', data);
                        $('#tombol-simpan-menu').html('Simpan');
                        toastr.error('Data Gagal Disimpan'); //tampilkan toastr dengan notif data gagal disimpan pada posisi kiri bawah
                    }
                });
            }
        })
    }

</script>

<!-- JAVASCRIPT -->
@endsection