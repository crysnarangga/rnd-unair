@extends('partials.index')
@section('title', 'User Activity')

@section('content')
<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
    <!--begin::Subheader-->
    <div class="subheader subheader-solid" id="kt_subheader">
        <div class="container-fluid " style="display: flex;justify-content: flex-end;">
            <span class="pull-right" id="date"></span>&nbsp;&nbsp;         
            <span class="pull-right" id="time"></span>
        </div>
        <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <!--begin::Info-->
            <div class="d-flex align-items-center flex-wrap mr-2" style="column-gap: 3px">
                <!--begin::Page Title-->
                <span class="text-muted font-weight-bold mr-4">
                    <i class="fa-solid fa-person-chalkboard text-warning"></i>
                </span>
                <!--end::Page Title-->
                <!--begin::Actions-->
                <div class="subheader-separator subheader-separator-ver mt-2 mb-2 mr-4 bg-gray-200"></div>
                <h5 class="text-dark font-weight-bold mt-2 mb-2 mr-5">User Activity</h5>					
                <!--end::Actions-->
            </div>
            <!--end::Info-->
            <!--begin::Toolbar-->
            <!--end::Toolbar-->
        </div>
    </div>
    <!--end::Subheader-->
    		<!--begin::Entry-->
		<div class="d-flex flex-column-fluid">
			<!--begin::Container-->
			<div class="container-fluid">
				<!--begin::Notice-->
                <!--end::Notice-->
                <!--begin::Card-->
                <div class="card card-custom gutter-b">
                    <div class="card-header flex-wrap py-3">
                        <div class="card-title">
                            <h3 class="card-label">Data User Activity</h3>
                            <span class="d-block text-muted pt-2 font-size-sm"></span></h3>
                        </div>
                       
                    </div>
                    <div class="card-body">
                            <!--begin: Search Form-->
                            <!--begin::Search Form-->
                            <div class="mb-7">
                                <div class="row align-items-center">
                                    <div class="col-lg-9 col-xl-8">
                                        <div class="row align-items-center">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--end::Search Form-->
                            <!--end: Search Form-->
                            <!--begin: Datatable-->
                            <table class="table table-striped table-responsive" id="table_activity">
                                <thead>
                                    <tr>
                                        <th>No Activity</th>
                                        <th>Id User</th>
                                        <th>Discripsi</th>
                                        <th>Status</th>
                                        <th>Menu ID</th>
                                        <th>Created At</th>
                                    </tr>
                                </thead>
                            </table>
                            <!--end: Datatable-->
                    </div>
                </div>
                <!--end::Card-->
			</div>
			<!--end::Container-->
		</div>
		<!--end::Entry-->
    </div>
    <!--end::Content-->

    <!--begin::Modal - Create App-->
		<div class="modal fade" id="tambah-edit-modal" tabindex="-1" aria-hidden="true">
			<!--begin::Modal dialog-->
			<div class="modal-dialog modal-dialog-centered mw-900px modal-dialog-scrollable">
				<!--begin::Modal content-->
				<div class="modal-content">
					<!--begin::Modal header-->
					<div class="modal-header">
						<!--begin::Modal title-->
						<h2 id="modal-judul"></h2>
						<!--end::Modal title-->
						<!--begin::Close-->
						<div class="btn btn-sm btn-icon btn-active-color-primary" data-bs-dismiss="modal">
							<!--begin::Svg Icon | path: icons/duotune/arrows/arr061.svg-->
							<span class="svg-icon svg-icon-1">
								<svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
									<rect opacity="0.5" x="6" y="17.3137" width="16" height="2" rx="1" transform="rotate(-45 6 17.3137)" fill="currentColor" />
									<rect x="7.41422" y="6" width="16" height="2" rx="1" transform="rotate(45 7.41422 6)" fill="currentColor" />
								</svg>
							</span>
							<!--end::Svg Icon-->
						</div>
						<!--end::Close-->
					</div>
					<!--end::Modal header-->
					<!--begin::Modal body-->
					<div class="modal-body py-lg-10 px-lg-10">
						<!--begin:Form-->
                        <form role="form" class="form" id="form-tambah-edit" name="form-tambah-edit" enctype="multipart/formdata" method="">
                            <div class="modal-body" style="height: 300px;">
                                <div class="mb-7">
                                    <input type="hidden" name="id" id="id">
                                    <div class="form-group row">
                                        <label class="col-lg-3 col-form-label">Jenis User</label>
                                        <div class="col-lg-9">
                                            <input type="text" class="form-control" id="jenis_user" name="jenis_user" placeholder="e.g: Dashboard" value="" required/>
                                            <span class="form-text text-muted">Masukan Jenis User</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="submit" id="tombol-simpan" value="create" class="btn btn-primary font-weight-bold">
                                    <i class="fa fa-save"></i> Save changes
                                </button>
                            </div>
                        </form>
                        <!--end:Form-->
					</div>
					<!--end::Modal body-->
				</div>
				<!--end::Modal content-->
			</div>
			<!--end::Modal dialog-->
		</div>

<!-- MULAI MODAL KONFIRMASI DELETE-->

<div class="modal fade" tabindex="-1" role="dialog" id="konfirmasi-modal" data-backdrop="false">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">PERHATIAN</h5>
                <div class="btn btn-sm btn-icon btn-active-color-primary" data-bs-dismiss="modal">
                    <!--begin::Svg Icon | path: icons/duotune/arrows/arr061.svg-->
                    <span class="svg-icon svg-icon-1">
                        <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <rect opacity="0.5" x="6" y="17.3137" width="16" height="2" rx="1" transform="rotate(-45 6 17.3137)" fill="currentColor" />
                            <rect x="7.41422" y="6" width="16" height="2" rx="1" transform="rotate(45 7.41422 6)" fill="currentColor" />
                        </svg>
                    </span>
                    <!--end::Svg Icon-->
                </div>
            </div>
            <div class="modal-body">
                <p><b>Jika menghapus Menu maka</b></p>
                <p>*data menu tersebut hilang selamanya, apakah anda yakin?</p>
            </div>
            <div class="modal-footer bg-whitesmoke br">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                <button type="button" class="btn btn-danger" name="tombol-hapus" id="tombol-hapus">Hapus
                    Data</button>
            </div>
        </div>
    </div>
</div>
@endsection
  @section('js_page')y8.eee
  <!-- LIBARARY JS -->
  <script type="text/javascript" language="javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>


<script type="text/javascript" language="javascript"
    src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>


<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.min.js"
    integrity="sha256-sPB0F50YUDK0otDnsfNHawYmA5M0pjjUf4TvRJkGFrI=" crossorigin="anonymous"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js" 
    integrity="sha512-VEd+nq25CkR676O+pLBnDW09R7VQX9Mdiij052gVCp5yVH3jGtH70Ho/UUv4mJDsEdTvqRCFZg0NKGiojGnUCw==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
  
  
  <!-- AKHIR LIBARARY JS -->
  
<!-- AKHIR LIBARARY JS -->

 <!-- JAVASCRIPT -->
 <script>
    //CSRF TOKEN PADA HEADER
    //Script ini wajib krn kita butuh csrf token setiap kali mengirim request post, patch, put dan delete ke server
    $(document).ready(function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    });

 
    //MULAI DATATABLE
    //script untuk memanggil data json dari server dan menampilkannya berupa datatable
    $(document).ready(function () {
        $('#table_activity').DataTable({
            processing: true,
            serverSide: true, //aktifkan server-side 
            ajax: {
                url: './useractivity/list', //url untuk request data
                type: 'GET'
            },
            columns: [
                {
                    data: 'no_activity',
                    name: 'No Activity'
                },
                {
                    data: 'nama_user',
                    name: 'Id User '
                },
                {
                    data: 'discripsi',
                    name: 'Discripsi'
                },
                {
                    data: 'status',
                    name: 'Status',
                    render: function (data, type, row, meta) {
                        if (data == 'Success') {
                            return '<span class="badge badge-success">Success</span>';
                        } else {
                            return '<span class="badge badge-danger">Failed</span>';
                        }
                    }
                },
                {
                    data: 'menu_id',
                    name: 'Menu ID',
                },
                {
                    data: 'created_at',
                    name: 'Created At',
                    render: function (data, type, row, meta) {
                        return moment(data).format('DD-MM-YYYY HH:mm:ss');
                    }
                },
            ],
            order: [
                [0, 'asc']
            ]
        });
    });

</script>
  
   <!-- JAVASCRIPT -->
   
  
  <!-- JAVASCRIPT -->
  @endsection