@extends('partials.index')
@section('title', 'Error Application')

@section('content')
<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
    <!--begin::Subheader-->
    <div class="subheader subheader-solid" id="kt_subheader">
        <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <!--begin::Info-->
            <div class="d-flex align-items-center flex-wrap mr-2" style="column-gap: 3px">
                <!--begin::Page Title-->
                <span class="text-muted font-weight-bold mr-4">
                    <i class="fa fa-layer-group text-primary"></i>
                </span>
                <!--end::Page Title-->
                <!--begin::Actions-->
                <div class="subheader-separator subheader-separator-ver mt-2 mb-2 mr-4 bg-gray-200"></div>
                <h5 class="text-dark font-weight-bold mt-2 mb-2 mr-5">Error Application</h5>					
                <!--end::Actions-->
            </div>
            <!--end::Info-->
            <!--begin::Toolbar-->
            <!--end::Toolbar-->
        </div>
    </div>
    <!--end::Subheader-->
    		<!--begin::Entry-->
		<div class="d-flex flex-column-fluid">
			<!--begin::Container-->
			<div class="container-fluid">
				<!--begin::Notice-->
                <!--end::Notice-->
                <!--begin::Card-->
                <div class="card card-custom gutter-b">
                    <div class="card-header flex-wrap py-3">
                        <div class="card-title">
                            <h3 class="card-label">Data Error Application</h3>
                            <span class="d-block text-muted pt-2 font-size-sm"></span></h3>
                        </div>
                       
                    </div>
                    <div class="card-body">
                            <!--begin: Search Form-->
                            <!--begin::Search Form-->
                            <div class="mb-7">
                                <div class="row align-items-center">
                                    <div class="col-lg-9 col-xl-8">
                                        <div class="row align-items-center">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--end::Search Form-->
                            <!--end: Search Form-->
                            <!--begin: Datatable-->
                            <table class="table table-striped responsive nowrap" id="table_error" width="100%" style="overflow-x: auto; display: block;">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Id User</th>
                                        <th>Error Date</th>
                                        <th>Modules</th>
                                        <th>Controller</th>
                                        <th>Function</th>
                                        <th>Error Line</th>
                                        <th>Error Message</th>
                                        <th>Status</th>
                                        <th>Param</th>
                                    </tr>
                                </thead>
                            </table>
                            <!--end: Datatable-->
                    </div>
                </div>
                <!--end::Card-->
			</div>
			<!--end::Container-->
		</div>
		<!--end::Entry-->
    </div>
    <!--end::Content-->

<!-- MULAI MODAL KONFIRMASI DELETE-->

<div class="modal fade" tabindex="-1" role="dialog" id="konfirmasi-modal" data-backdrop="false">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">PERHATIAN</h5>
                <div class="btn btn-sm btn-icon btn-active-color-primary" data-bs-dismiss="modal">
                    <!--begin::Svg Icon | path: icons/duotune/arrows/arr061.svg-->
                    <span class="svg-icon svg-icon-1">
                        <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <rect opacity="0.5" x="6" y="17.3137" width="16" height="2" rx="1" transform="rotate(-45 6 17.3137)" fill="currentColor" />
                            <rect x="7.41422" y="6" width="16" height="2" rx="1" transform="rotate(45 7.41422 6)" fill="currentColor" />
                        </svg>
                    </span>
                    <!--end::Svg Icon-->
                </div>
            </div>
            <div class="modal-body">
                <p><b>Jika menghapus Menu maka</b></p>
                <p>*data menu tersebut hilang selamanya, apakah anda yakin?</p>
            </div>
            <div class="modal-footer bg-whitesmoke br">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                <button type="button" class="btn btn-danger" name="tombol-hapus" id="tombol-hapus">Hapus
                    Data</button>
            </div>
        </div>
    </div>
</div>
@endsection
  @section('js_page')
  <!-- LIBARARY JS -->
  <script type="text/javascript" language="javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
  
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
      integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous">
  </script>
  
  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
      integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous">
  </script>
  
  <script type="text/javascript" language="javascript"
      src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
  
  <script type="text/javascript" language="javascript"
      src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
  
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.min.js"
      integrity="sha256-sPB0F50YUDK0otDnsfNHawYmA5M0pjjUf4TvRJkGFrI=" crossorigin="anonymous"></script>
  
  <script src="https://cdnjs.cloudflare.com/ajax/libs/izitoast/1.4.0/js/iziToast.js"
      integrity="sha256-siqh9650JHbYFKyZeTEAhq+3jvkFCG8Iz+MHdr9eKrw=" crossorigin="anonymous"></script>
  
  
  <!-- AKHIR LIBARARY JS -->
  
<!-- AKHIR LIBARARY JS -->

 <!-- JAVASCRIPT -->
 <script>
    //CSRF TOKEN PADA HEADER
    //Script ini wajib krn kita butuh csrf token setiap kali mengirim request post, patch, put dan delete ke server
    $(document).ready(function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    });

 
    //MULAI DATATABLE
    //script untuk memanggil data json dari server dan menampilkannya berupa datatable
    $(document).ready(function () {
        $('#table_error').DataTable({
            processing: true,
            serverSide: true, //aktifkan server-side 
            ajax: {
                url: './errorapplication/list', //url untuk request data
                type: 'GET'
            },
            columns: [
                {
                    data: 'id',
                    name: 'ID'
                },
                {
                    data: 'id_user',
                    name: 'Id User '
                },
                {
                    data: 'error_date',
                    name: 'Error Date'
                },
                {
                    data: 'modules',
                    name: 'Modules'
                },
                {
                    data: 'controller',
                    name: 'Controller'
                },
                {
                    data: 'function',
                    name: 'Function'
                },
                {
                    data: 'error_line',
                    name: 'Error Line'
                },
                {
                    data: 'error_message',
                    name: 'Error Message'
                },
                {
                    data: 'status',
                    name: 'Status',
                },
                {
                    data: 'param',
                    name: 'Param',
                },
            ],
            order: [
                [0, 'asc']
            ]
        });
    });

</script>
  
   <!-- JAVASCRIPT -->
   
  
  <!-- JAVASCRIPT -->
  @endsection