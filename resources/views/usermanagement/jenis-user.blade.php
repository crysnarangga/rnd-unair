@extends('partials.index')
@section('title', 'Jenis User')

@section('content')
<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
    <!--begin::Subheader-->
    <div class="subheader subheader-solid" id="kt_subheader">
        <div class="container-fluid " style="display: flex;justify-content: flex-end;">
            <span class="pull-right" id="date"></span>&nbsp;&nbsp;         
            <span class="pull-right" id="time"></span>
        </div>
        <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <!--begin::Info-->
            <div class="d-flex align-items-center flex-wrap mr-2" style="column-gap: 3px">
                <!--begin::Page Title-->
                <span class="text-muted font-weight-bold mr-4">
                    <i class="fa-solid fa-users text-warning"></i>
                </span>
                <!--end::Page Title-->
                <!--begin::Actions-->
                <div class="subheader-separator subheader-separator-ver mt-2 mb-2 mr-4 bg-gray-200"></div>
                <h5 class="text-dark font-weight-bold mt-2 mb-2 mr-5">Jenis User</h5>					
                <!--end::Actions-->
            </div>
            <!--end::Info-->
            <!--begin::Toolbar-->
            <!--end::Toolbar-->
        </div>
    </div>
    <!--end::Subheader-->
    		<!--begin::Entry-->
		<div class="d-flex flex-column-fluid">
			<!--begin::Container-->
			<div class="container-fluid">
				<!--begin::Notice-->
                <!--end::Notice-->
                <!--begin::Card-->
                <div class="card card-custom gutter-b">
                    <div class="card-header flex-wrap py-3">
                        <div class="card-title">
                            <h3 class="card-label">Data Jenis User
                            <span class="d-block text-muted pt-2 font-size-sm"></span></h3>
                        </div>
                        <div class="card-toolbar">
                            <!--begin::Button-->
                            <a id="tombol-tambah" name="addMenu" class="btn btn-primary font-weight-bolder" href="javascript:void(0)">
                                <span class="svg-icon svg-icon-md">
                                    <!--begin::Svg Icon | path:assets/media/svg/icons/Design/Flatten.svg-->
                                    <!--end::Svg Icon-->
                                </span>+ Tambah</a>
                            </a>
                            <!--end::Button-->
                        </div>
                    </div>
                    <div class="card-body">
                            <!--begin: Search Form-->
                            <!--begin::Search Form-->
                            <!--end::Search Form-->
                            <!--end: Search Form-->
                            <!--begin: Datatable-->
                            <table class="table table-striped table-responsive" id="table_menu">
                                <thead>
                                    <tr>
                                        <th>Jenis User</th>
                                        <th style="width: 200px">Action</th>
                                    </tr>
                                </thead>
                            </table>
                            <!--end: Datatable-->
                    </div>
                </div>
                <!--end::Card-->
			</div>
			<!--end::Container-->
		</div>
		<!--end::Entry-->
    </div>
    <!--end::Content-->

    <!--begin::Modal - Create App-->
		<div class="modal fade" id="tambah-edit-modal" tabindex="-1" aria-hidden="true">
			<!--begin::Modal dialog-->
			<div class="modal-dialog modal-dialog-centered mw-700px modal-dialog-scrollable">
				<!--begin::Modal content-->
				<div class="modal-content">
					<!--begin::Modal header-->
					<div class="modal-header" style="background-color: #153253">
						<!--begin::Modal title-->
						<h2 id="modal-judul" style="color: white"></h2>
						<!--end::Modal title-->
						<!--begin::Close-->
						<div class="btn btn-sm btn-icon btn-active-color-primary" data-bs-dismiss="modal">
							<!--begin::Svg Icon | path: icons/duotune/arrows/arr061.svg-->
							<span class="svg-icon svg-icon-1">
								<svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
									<rect opacity="0.5" x="6" y="17.3137" width="16" height="2" rx="1" transform="rotate(-45 6 17.3137)" fill="currentColor" />
									<rect x="7.41422" y="6" width="16" height="2" rx="1" transform="rotate(45 7.41422 6)" fill="currentColor" />
								</svg>
							</span>
							<!--end::Svg Icon-->
						</div>
						<!--end::Close-->
					</div>
					<!--end::Modal header-->
					<!--begin::Modal body-->
					<div class="modal-body py-lg-10 px-lg-10">
						<!--begin:Form-->
                        <form role="form" class="form" id="form-tambah-edit" name="form-tambah-edit" enctype="multipart/formdata" method="">
                            <div class="modal-body" style="height: 100px;">
                                <div class="mb-7">
                                    <input type="hidden" name="id" id="id">
                                    <div class="form-group row">
                                        <label class="col-lg-3 col-form-label">Jenis User</label>
                                        <div class="col-lg-9">
                                            <input type="text" class="form-control" id="jenis_user" name="jenis_user" placeholder="e.g: Dashboard" value="" required/>
                                            <span class="form-text text-muted">Masukan Jenis User</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="submit" id="tombol-simpan" value="create" class="btn btn-primary font-weight-bold">
                                    <i class="fa fa-save"></i> Save changes
                                </button>
                            </div>
                        </form>
                        <!--end:Form-->
					</div>
					<!--end::Modal body-->
				</div>
				<!--end::Modal content-->
			</div>
			<!--end::Modal dialog-->
		</div>

<!-- MULAI MODAL KONFIRMASI DELETE-->

<div class="modal fade" tabindex="-1" role="dialog" id="konfirmasi-modal" data-backdrop="false">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">PERHATIAN</h5>
                <div class="btn btn-sm btn-icon btn-active-color-primary" data-bs-dismiss="modal">
                    <!--begin::Svg Icon | path: icons/duotune/arrows/arr061.svg-->
                    <span class="svg-icon svg-icon-1">
                        <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <rect opacity="0.5" x="6" y="17.3137" width="16" height="2" rx="1" transform="rotate(-45 6 17.3137)" fill="currentColor" />
                            <rect x="7.41422" y="6" width="16" height="2" rx="1" transform="rotate(45 7.41422 6)" fill="currentColor" />
                        </svg>
                    </span>
                    <!--end::Svg Icon-->
                </div>
            </div>
            <div class="modal-body">
                <p><b>Jika menghapus Menu maka</b></p>
                <p>*data menu tersebut hilang selamanya, apakah anda yakin?</p>
            </div>
            <div class="modal-footer bg-whitesmoke br">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                <button type="button" class="btn btn-danger" name="tombol-hapus" id="tombol-hapus">Hapus
                    Data</button>
            </div>
        </div>
    </div>
</div>

<!-- Mulai modal menu --->
<div class="modal fade" id="menu-modal" tabindex="-1" aria-hidden="true">
    <!--begin::Modal dialog-->
    <div class="modal-dialog modal-dialog-centered mw-500px modal-dialog-scrollable">
        <!--begin::Modal content-->
        <div class="modal-content">
            <!--begin::Modal header-->
            <div class="modal-header" style="background-color: #153253">
                <!--begin::Modal title-->
                <h2 id="modal-judul" style="color: white">Setting Menu User</h2>
                <!--end::Modal title-->
                <!--begin::Close-->
                <div class="btn btn-sm btn-icon btn-active-color-primary" data-bs-dismiss="modal">
                    <!--begin::Svg Icon | path: icons/duotune/arrows/arr061.svg-->
                    <span class="svg-icon svg-icon-1">
                        <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <rect opacity="0.5" x="6" y="17.3137" width="16" height="2" rx="1" transform="rotate(-45 6 17.3137)" fill="currentColor" />
                            <rect x="7.41422" y="6" width="16" height="2" rx="1" transform="rotate(45 7.41422 6)" fill="currentColor" />
                        </svg>
                    </span>
                    <!--end::Svg Icon-->
                </div>
                <!--end::Close-->
            </div>
            <!--end::Modal header-->
            <!--begin::Modal body-->
            <div class="modal-body py-lg-10 px-lg-10">
                <!--begin:Form-->
                <form role="form" class="form" id="form-tambah-menu" name="form-tambah-edit" enctype="multipart/form-data" method="">
                    <div class="modal-body" style="height: 300px;">
                        <input type="hidden" name="idjenisuser" id="idjenisuser" value="">
                        @php
                            foreach ($menu as $key => $value) {
                                $count = 0;
                                if ($value->id_level == '1') {
                                    echo '<span class="text-primary h6">'.Str::upper($value->menu_name).'</span>';
                                    echo '<input type="checkbox" class="custom-control-input ok'.$value->id.'" id="'.$value->id.'" name="menu[]" value="'.$value->id.'" style="display:hidden; height: 0; width: 0">';
                                    foreach ($menu as $key1 => $value1) {
                                        if ($value1->parent_id == $value->id) {
                                            echo '<div class="form-group row">';
                                                echo '<div class="col-lg-5">';
                                                    echo '<i class="fa-solid fa-caret-right text-primary">&nbsp;</i>';
                                                    echo '<label class="col-form-label">&nbsp;&nbsp;&nbsp;'.$value1->menu_name.'</label>';
                                                echo '</div>';
                                                echo '<div class="col-lg-7">';
                                                    echo '<div class="custom-control custom-checkbox p-4">';
                                                        echo '<input type="checkbox" class="custom-control-input ok'.$value1->id.'" id="'.$value1->id.'" name="menu[]" value="'.$value1->id.'">';
                                                        echo '<label class="custom-control-label" for="'.$value1->id.'"></label>';
                                                    echo '</div>';
                                                echo '</div>';
                                            echo '</div>';
                                            $count++;
                                            echo "<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.1/jquery.min.js' integrity='sha512-aVKKRRi/Q/YV+4mjoKBsE4x3H+BkegoM/em46NNlCqNTmUYADjBbeNefNxYV7giUp0VxICtqdrbqU7iVaeZNXA==' crossorigin='anonymous' referrerpolicy='no-referrer'></script>";
                                            echo "<script>
                                                var chk1 = $('.ok".$value1->id."');
                                                var chk2 = $('.ok".$value->id."');

                                                chk1.on('click', function(){
                                                if( chk1.is(':checked') ) {
                                                    chk2.attr('checked', true);
                                                } else {
                                                    chk2.attr('checked', false);
                                                }
                                                });

                                                // var checkbox1 = document.getElementsByClassName('ok".$value1->id."');
                                                // var checkbox2 = document.getElementsByClassName('ok".$value->id."');
                                                // function myFunction(){
                                                //     var isChecked = checkbox1.checked;
                                                //     if(isChecked){
                                                //         checkbox2.checked = true;
                                                //     }
                                                // }
                                            </script>";
                                        }
                                    }
                                    if ($count == 0) {
                                        echo '<input type="checkbox" class="custom-control-input ok'.$value->id.'" id="'.$value->id.'" name="menu[]" value="'.$value->id.'" style="display:inline-block; margin-left: 83px;">';
                                        echo '<label class="custom-control-label" for="'.$value->id.'"></label>';
                                        echo '<br>';
                                    }
                                }
                            }
                        @endphp
                    </div>
                    <div class="modal-footer">
                        <button type="submit" id="tombol-simpan-menu" value="create" class="btn btn-primary font-weight-bold">
                            <i class="fa fa-save"></i> Save changes
                        </button>
                    </div>
                </form>
                <!--end:Form-->
            </div>
            <!--end::Modal body-->
        </div>
        <!--end::Modal content-->
    </div>
    <!--end::Modal dialog-->
</div>
<!-- Akhir modal menu -->

@endsection
  @section('js_page')
  <!-- LIBARARY JS -->
  <script type="text/javascript" language="javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
  
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
      integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous">
  </script>
  
  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
      integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous">
  </script>
  
  <script type="text/javascript" language="javascript"
      src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
  
  <script type="text/javascript" language="javascript"
      src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
  
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.min.js"
      integrity="sha256-sPB0F50YUDK0otDnsfNHawYmA5M0pjjUf4TvRJkGFrI=" crossorigin="anonymous"></script>
  
  <script src="https://cdnjs.cloudflare.com/ajax/libs/izitoast/1.4.0/js/iziToast.js"
      integrity="sha256-siqh9650JHbYFKyZeTEAhq+3jvkFCG8Iz+MHdr9eKrw=" crossorigin="anonymous"></script>
  
  
  <!-- AKHIR LIBARARY JS -->
  
   <!-- JAVASCRIPT -->
   <script>
      //CSRF TOKEN PADA HEADER
      //Script ini wajib krn kita butuh csrf token setiap kali mengirim request post, patch, put dan delete ke server
      $(document).ready(function () {
          $.ajaxSetup({
              headers: {
                  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              }
          });
      });
  
      //TOMBOL TAMBAH DATA
      //jika tombol-tambah diklik maka
      $('#tombol-tambah').click(function () {
          $('#button-simpan').val("create-post"); //valuenya menjadi create-post
          $('#id').val(''); //valuenya menjadi kosong
          $('#form-tambah-edit').trigger("reset"); //mereset semua input dll didalamnya
          $('#modal-judul').html("Tambah Jenis User"); //valuenya tambah pegawai baru
          $('#tambah-edit-modal').modal('show'); //modal tampil
      });
  
      //MULAI DATATABLE
      //script untuk memanggil data json dari server dan menampilkannya berupa datatable
      $(document).ready(function () {
          $('#table_menu').DataTable({
              processing: true,
              serverSide: true, //aktifkan server-side 
              ajax: {
                  url: './jenisuser/list', //url untuk request data
                  type: 'GET'
              },
              columns: [
                  {
                      data: 'jenis_user',
                      name: 'Jenis User'
                  },
       
                  {
                      data: 'action',
                      name: 'Action',
                      orderable: false,
                      searchable: false
                  },
              ],
              order: [
                  [0, 'asc']
              ]
          });
      });
  
      //SIMPAN & UPDATE DATA DAN VALIDASI (SISI CLIENT)
      //jika id = form-tambah-edit panjangnya lebih dari 0 atau bisa dibilang terdapat data dalam form tersebut maka
      //jalankan jquery validator terhadap setiap inputan dll dan eksekusi script ajax untuk simpan data
      if ($("#form-tambah-edit").length > 0) {
          $("#form-tambah-edit").validate({
              submitHandler: function (form) {
                  var actionType = $('#tombol-simpan').val();
                  $('#tombol-simpan').html('Sending..');
  
                  $.ajax({
                      data: $('#form-tambah-edit')
                          .serialize(), //function yang dipakai agar value pada form-control seperti input, textarea, select dll dapat digunakan pada URL query string ketika melakukan ajax request
                      url: "{{ route('jenisuser.store') }}", //url simpan data
                      type: "POST", //karena simpan kita pakai method POST
                      dataType: 'json', //data tipe kita kirim berupa JSON
                      success: function (data) { //jika berhasil 
                          $('#form-tambah-edit').trigger("reset"); //form reset
                          $('#tambah-edit-modal').modal('hide'); //modal hide
                          $('#tombol-simpan').html('Simpan'); //tombol simpan
                          var oTable = $('#table_menu').dataTable(); //inialisasi datatable
                          oTable.fnDraw(false); //reset datatable
                          iziToast.success({ //tampilkan iziToast dengan notif data berhasil disimpan pada posisi kanan bawah
                              title: 'Data Berhasil Disimpan',
                              message: '{{ Session('
                              success ')}}',
                              position: 'bottomRight'
                          });
                      },
                      error: function (data) { //jika error tampilkan error pada console
                          console.log('Error:', data);
                          $('#tombol-simpan').html('Simpan');
                      }
                  });
              }
          })
      }
  
      //TOMBOL EDIT DATA PER PEGAWAI DAN TAMPIKAN DATA BERDASARKAN ID PEGAWAI KE MODAL
      //ketika class edit-post yang ada pada tag body di klik maka
      $('body').on('click', '.edit-post', function () {
          var data_id = $(this).data('id');
          $.get('jenisuser/' + data_id + '/edit', function (data) {
              $('#modal-judul').html("Edit Post");
              $('#tombol-simpan').val("edit-post");
              $('#tambah-edit-modal').modal('show');
  
              //set value masing-masing id berdasarkan data yg diperoleh dari ajax get request diatas               
              $('#id').val(data.id);
              $('#jenis_user').val(data.jenis_user);
            
          })
      });
  
      //jika klik class delete (yang ada pada tombol delete) maka tampilkan modal konfirmasi hapus maka
      $(document).on('click', '.delete', function () {
          dataId = $(this).attr('id');
          $('#konfirmasi-modal').modal('show');
      });
  
      //jika tombol hapus pada modal konfirmasi di klik maka
      $('#tombol-hapus').click(function () {
          $.ajax({
  
              url: "jenisuser/" + dataId, //eksekusi ajax ke url ini
              type: 'delete',
              beforeSend: function () {
                  $('#tombol-hapus').text('Hapus Data'); //set text untuk tombol hapus
              },
              success: function (data) { //jika sukses
                  setTimeout(function () {
                      $('#konfirmasi-modal').modal('hide'); //sembunyikan konfirmasi modal
                      var oTable = $('#table_menu').dataTable();
                      oTable.fnDraw(false); //reset datatable
                  });
                  iziToast.warning({ //tampilkan izitoast warning
                      title: 'Data Berhasil Dihapus',
                      message: '{{ Session('
                      delete ')}}',
                      position: 'bottomRight'
                  });
              }
          })
      });

      $('body').on('click', '.menu-post', function () {
        var data_id = $(this).data('id');
        $('#form-tambah-menu').trigger("reset"); //form reset
        $.get('jenisUser/' + data_id + '/listmenu', function (data) {
            $('#modal-judul').html("Setting Menu");
            $('#menu-modal').modal('show');
            $.each(data, function (i, item) {
                $('.ok' + item.menu_id).prop('checked', true);
            });
            $('#idjenisuser').val(data_id);
            toastr.info('Data Berhasil Diambil'); //tampilkan toastr dengan notif data berhasil diedit pada posisi kiri bawah
        })
    });

    if ($("#form-tambah-menu").length > 0) {
        $("#form-tambah-menu").validate({
            submitHandler: function (form) {
                event.preventDefault();
                var actionType = $('#tombol-simpan-menu').val();
                $('#tombol-simpan').html('Sending..');
                var form = $('#form-tambah-menu')[0];
                var formData = new FormData(form);

                $.ajax({
                    data: formData, //function yang dipakai agar value pada form-control seperti input, textarea, select dll dapat digunakan pada URL query string ketika melakukan ajax request
                    url: './jenisuser/storemenu', //url simpan data
                    type: "POST", //karena simpan kita pakai method POST
                    dataType: 'json', //data tipe kita kirim berupa JSON
                    processData: false,
                    contentType: false,
                    success: function (data) { //jika berhasil 
                        $('#form-tambah-menu').trigger("reset"); //form reset
                        $('#menu-modal').modal('hide'); //modal hide
                        $('#tombol-simpan-menu').html('Simpan'); //tombol simpan
                        var oTable = $('#table_menu').dataTable(); //inialisasi datatable
                        oTable.fnDraw(false); //reset datatable
                        iziToast.success({ //tampilkan iziToast dengan notif data berhasil disimpan pada posisi kanan bawah
                            title: 'Data Berhasil Disimpan',
                            message: '{{ Session('
                            success ')}}',
                            position: 'bottomRight'
                        });
                        toastr.success('Data Berhasil Disimpan'); //tampilkan toastr dengan notif data berhasil disimpan pada posisi kiri bawah
                    },
                    error: function (data) { //jika error tampilkan error pada console
                        console.log('Error:', data);
                        $('#tombol-simpan-menu').html('Simpan');
                        toastr.error('Data Gagal Disimpan'); //tampilkan toastr dengan notif data gagal disimpan pada posisi kiri bawah
                    }
                });
            }
        })
    }


  
  </script>
  
  <!-- JAVASCRIPT -->
  @endsection