@extends('partials.index')
@section('title', 'Profile')
@section('content')
<style>
.img-account-profile {
    height: 10rem;
    width: 10rem;
}
.rounded-circle {
    border-radius: 50% !important;
}
.card {
    box-shadow: 0 0.15rem 1.75rem 0 rgb(33 40 50 / 15%);
}
.card .card-header {
    font-weight: 500;
}
.card-header:first-child {
    border-radius: 0.35rem 0.35rem 0 0;
}
.card-header {
    padding: 1rem 1.35rem;
    margin-bottom: 0;
    background-color: rgba(33, 40, 50, 0.03);
    border-bottom: 1px solid rgba(33, 40, 50, 0.125);
}
.form-control, .dataTable-input {
    display: block;
    width: 100%;
    padding: 0.875rem 1.125rem;
    font-size: 0.875rem;
    font-weight: 400;
    line-height: 1;
    color: #69707a;
    background-color: #fff;
    background-clip: padding-box;
    border: 1px solid #c5ccd6;
    -webkit-appearance: none;
    -moz-appearance: none;
    appearance: none;
    border-radius: 0.35rem;
    transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
}
</style>
@if (session()->has('success'))
    <script>
        toastr.success("{{ session()->get('success') }}");
    </script>
@endif
<div class="container-xl px-4 mt-4">
    <div class="row">
        <div class="col-xl-4">
            <!-- Profile picture card-->
            <form method="POST" action="/profile/updatefoto" enctype="multipart/form-data">
            @csrf
            <div class="card mb-4 mb-xl-0">
                <div class="card-header mx-auto p-4 h4">Profile Picture</div>
                <div class="card-body text-center">
                    <!-- Profile picture image-->
                    <img class="img-account-profile rounded-circle mb-2" src="assets/media/img/profile/{{ $foto }}" alt="user">
                    <!-- Profile picture help block-->
                    <div class="small font-italic text-muted mb-4">JPG or PNG no larger than 5 MB</div>
                    <!-- Profile picture upload button-->
                    <div style="height:0px;overflow:hidden">
                        <input type="file" id="photo" name="photo" />
                    </div>
                    <button class="btn btn-primary" type="button" onclick="chooseFile();">Upload new image</button>
                    <script>
                        function chooseFile() {
                           $("#photo").click();
                           // auto submit form after file is selected
                            $("#photo").change(function() {
                                 $(this).closest("form").submit();
                            });
                        }
                     </script>
                </div>
            </div>
            </form>
        </div>
        <div class="col-xl-8">
            <!-- Account details card-->
            <div class="card mb-4">
                <div class="card-header mx-auto p-4 h4">Profile Details</div>
                <div class="card-body">
                    <form method="POST" action="/profile/updateprofile">
                        @csrf
                        <!-- Form Group (username)-->
                        <div class="mb-3">
                            <label class="small mb-1" for="inputUsername">Username</label>
                            <input class="form-control" id="inputUsername" type="text" placeholder="Enter your username" value="{{ Auth::user()->username }}" name="username">
                        </div>
                        <!-- Form Row-->
                        <div class="mb-3">
                            <label class="small mb-1" for="inputNamaUser">Nama User</label>
                            <input class="form-control" id="inputNamaUser" type="text" placeholder="Enter your Nama User" value="{{ Auth::user()->nama_user }}" name="nama_user">
                        </div>
                        <!-- Form Group (email address)-->
                        <div class="mb-3">
                            <label class="small mb-1" for="inputEmailAddress">Email address</label>
                            <input class="form-control" id="inputEmailAddress" type="email" placeholder="Enter your email address" value="{{ Auth::user()->email }}" name="email">
                        </div>
                        <div class="mb-3">
                            <label class="small mb-1" for="inputPhone">Phone numbers</label>
                            <input class="form-control" id="inputPhone" type="tel" placeholder="Enter your phone number" value="{{ Auth::user()->no_hp }}" name="no_hp">
                        </div>
                        <!-- Form Row        -->
                        <div class="row gx-3 mb-3">
                            <!-- Form Group (organization name)-->
                            <div class="col-md-6">
                                <label class="small mb-1" for="inputNoWA">No Whatsapp</label>
                                <input class="form-control" id="inputNoWA" type="text" placeholder="Enter your No Whatsapp" value="{{ Auth::user()->wa }}" name="wa">
                            </div>
                            <!-- Form Group (location)-->
                            <div class="col-md-6">
                                <label class="small mb-1" for="inputPin">Pin</label>
                                <input class="form-control" id="inputPin" type="text" placeholder="Enter your Pin" value="{{ Auth::user()->pin }}" name="pin">
                            </div>
                        </div>
                        <!-- Save changes button-->
                        <input type="submit" class="btn btn-primary" value="Save Changes">
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>    
@endsection
