@foreach ($dashboard_menu as $item)
    @php
        $_active = ($loop->first) ? 'kt-menu__item--active' : '';
    @endphp
    @if (count($item['menu_childs']) == 0)
        <div class="menu-item {{ $_active }}">
            <a class="menu-link" href="{{ $item['menu_link'] }}">
                <span class="menu-icon">
                    <span class="svg-icon svg-icon-2">
                        <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <rect x="2" y="2" width="9" height="9" rx="2" fill="currentColor" />
                            <rect opacity="0.3" x="13" y="2" width="9" height="9" rx="2" fill="currentColor" />
                            <rect opacity="0.3" x="13" y="13" width="9" height="9" rx="2" fill="currentColor" />
                            <rect opacity="0.3" x="2" y="13" width="9" height="9" rx="2" fill="currentColor" />
                        </svg>
                    </span>                                
                </span>
                <span class="menu-title">{{ $item['menu_name'] }}</span>
            </a>
        </div>
    @else
        <div data-kt-menu-trigger="click" class="menu-item menu-accordion {{ $_active }}">
            <span class="menu-link">
                <span class="menu-icon">
                    <span class="svg-icon svg-icon-2">
                        <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <rect x="2" y="2" width="9" height="9" rx="2" fill="currentColor" />
                            <rect opacity="0.3" x="13" y="2" width="9" height="9" rx="2" fill="currentColor" />
                            <rect opacity="0.3" x="13" y="13" width="9" height="9" rx="2" fill="currentColor" />
                            <rect opacity="0.3" x="2" y="13" width="9" height="9" rx="2" fill="currentColor" />
                        </svg>
                    </span>                                
                </span>
                <span class="menu-title">{{ $item['menu_name'] }}</span>
                <span class="menu-arrow"></span>
            </span>
            <div class="menu-sub menu-sub-accordion">
                @foreach ($item['menu_childs'] as $child)
                    <div class="menu-item">
                        <a class="menu-link" href="{{ $child['menu_link'] }}">
                            <span class="menu-icon">
                                <span class="svg-icon svg-icon-2">
                                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <rect x="2" y="2" width="9" height="9" rx="2" fill="currentColor" />
                                        <rect opacity="0.3" x="13" y="2" width="9" height="9" rx="2" fill="currentColor" />
                                        <rect opacity="0.3" x="13" y="13" width="9" height="9" rx="2" fill="currentColor" />
                                        <rect opacity="0.3" x="2" y="13" width="9" height="9" rx="2" fill="currentColor" />
                                    </svg>
                                </span>                                       
                            </span>
                            <span class="menu-title">{{ $child['menu_name'] }}</span>
                        </a>
                    </div>
                @endforeach
            </div>
        </div>
    @endif
@endforeach