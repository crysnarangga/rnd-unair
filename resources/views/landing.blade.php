<!DOCTYPE html>
<!--
Author: Keenthemes
Product Name: Metronic - Bootstrap 5 HTML, VueJS, React, Angular & Laravel Admin Dashboard Theme 
Purchase: https://1.envato.market/EA4JP
Website: http://www.keenthemes.com
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Dribbble: www.dribbble.com/keenthemes
Like: www.facebook.com/keenthemes
License: For each use you must have a valid license purchased only from above link in order to legally use the theme for your project.
-->
<html lang="en" style="scroll-behavior: smooth">
	<!--begin::Head-->
	
<!-- Mirrored from preview.keenthemes.com/metronic8/demo1/landing.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 08 Jul 2022 06:29:31 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<head>
		<title>SISI SUPER APPS</title>
		<meta charset="utf-8" />
		<meta name="description" content="The most advanced Bootstrap Admin Theme on Themeforest trusted by 100,000 beginners and professionals. Multi-demo, Dark Mode, RTL support and complete React, Angular, Vue &amp; Laravel versions. Grab your copy now and get life-time updates for free." />
		<meta name="keywords" content="Metronic, bootstrap, bootstrap 5, Angular, VueJs, React, Laravel, admin themes, web design, figma, web development, free templates, free admin themes, bootstrap theme, bootstrap template, bootstrap dashboard, bootstrap dak mode, bootstrap button, bootstrap datepicker, bootstrap timepicker, fullcalendar, datatables, flaticon" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<meta property="og:locale" content="en_US" />
		<meta property="og:type" content="article" />
		<meta property="og:title" content="Metronic - Bootstrap 5 HTML, VueJS, React, Angular &amp; Laravel Admin Dashboard Theme" />
		<meta property="og:url" content="https://keenthemes.com/metronic" />
		<meta property="og:site_name" content="Keenthemes | Metronic" />
		<link rel="canonical" href="https://preview.keenthemes.com/metronic8" />
		<link rel="shortcut icon" href="assets/media/logos/sisilogosuperapp.png" />
		<!--begin::Fonts-->
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" />
		<!--end::Fonts-->
		<link rel="stylesheet"href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css"/>
		<link rel="stylesheet"href="https://cdn.jsdelivr.net/npm/swiper@8/swiper-bundle.min.css"/>

		<!--begin::Global Stylesheets Bundle(used by all pages)-->
		<link href="assets/plugins/global/plugins.bundle.css" rel="stylesheet" type="text/css" />
		<link href="assets/css/style.bundle.css" rel="stylesheet" type="text/css" />
		<!--end::Global Stylesheets Bundle-->
		<link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
		<!-- Link Swiper's CSS -->
		<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/swiper/swiper-bundle.min.css" />
		<link rel="stylesheet" media="screen and (min-width: 900px)" href="widescreen.css">
		<link rel="stylesheet" media="screen and (max-width: 600px)" href="smallscreen.css">
		<!--Begin::Google Tag Manager -->
		<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start': new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0], j=d.createElement(s),dl=l!='dataLayer'?'&amp;l='+l:'';j.async=true;j.src= '../../../www.googletagmanager.com/gtm5445.html?id='+i+dl;f.parentNode.insertBefore(j,f); })(window,document,'script','dataLayer','GTM-5FS8GGP');</script>
		<!--End::Google Tag Manager -->
	</head>
	<!--end::Head-->
	<!--begin::Body-->
	<body data-kt-name="metronic" id="kt_body" data-bs-spy="scroll" data-bs-target="#kt_landing_menu" class="bg-white position-relative app-blank">
		<!--begin::Theme mode setup on page load-->
		<script>if ( document.documentElement ) { const defaultThemeMode = "system"; const name = document.body.getAttribute("data-kt-name"); let themeMode = localStorage.getItem("kt_" + ( name !== null ? name + "_" : "" ) + "theme_mode_value"); if ( themeMode === null ) { if ( defaultThemeMode === "system" ) { themeMode = window.matchMedia("(prefers-color-scheme: dark)").matches ? "dark" : "light"; } else { themeMode = defaultThemeMode; } } document.documentElement.setAttribute("data-theme", themeMode); }</script>
		<!--end::Theme mode setup on page load-->
		<!--Begin::Google Tag Manager (noscript) -->
		<noscript>
			<iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5FS8GGP" height="0" width="0" style="display:none;visibility:hidden"></iframe>
		</noscript>
		<!--End::Google Tag Manager (noscript) -->
		<!--begin::Root-->
		<div class="d-flex flex-column flex-root" id="kt_app_root">
			<!--begin::Header Section-->
			<div class="mb-0" id="home">
				<!--begin::Wrapper-->
				<div class="bgi-no-repeat bgi-size-contain bgi-position-x-center bgi-position-y-bottom landing-dark-bg" style="background-image: url(assets/media/svg/illustrations/landing.svg)">
					<!--begin::Header-->
					<div class="landing-header" data-kt-sticky="true" data-kt-sticky-name="landing-header" data-kt-sticky-offset="{default: '200px', lg: '300px'}">
						<!--begin::Container-->
						<div class="container">
							<!--begin::Wrapper-->
							<div class="d-flex align-items-center justify-content-between">
								<!--begin::Logo-->
								<div class="d-flex align-items-center flex-equal">
									<!--begin::Mobile menu toggle-->
									<button class="btn btn-icon btn-active-color-primary me-3 d-flex d-lg-none" id="kt_landing_menu_toggle">
										<!--begin::Svg Icon | path: icons/duotune/abstract/abs015.svg-->
										<span class="svg-icon svg-icon-2hx">
											<svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
												<path d="M21 7H3C2.4 7 2 6.6 2 6V4C2 3.4 2.4 3 3 3H21C21.6 3 22 3.4 22 4V6C22 6.6 21.6 7 21 7Z" fill="currentColor" />
												<path opacity="0.3" d="M21 14H3C2.4 14 2 13.6 2 13V11C2 10.4 2.4 10 3 10H21C21.6 10 22 10.4 22 11V13C22 13.6 21.6 14 21 14ZM22 20V18C22 17.4 21.6 17 21 17H3C2.4 17 2 17.4 2 18V20C2 20.6 2.4 21 3 21H21C21.6 21 22 20.6 22 20Z" fill="currentColor" />
											</svg>
										</span>
										<!--end::Svg Icon-->
									</button>
									<!--end::Mobile menu toggle-->
									<!--begin::Logo image-->
									<a href="landing.html">
										<img alt="Logo" src="assets/media/logos/sisilogosuperapp.png" class="logo-default h-45px h-lg-60px" />
										<img alt="Logo" src="assets/media/logos/sisilogosuperapp.png" class="logo-sticky h-45px h-lg-60px" />
									</a>
									<!--end::Logo image-->
								</div>
								<!--end::Logo-->
								<!--begin::Menu wrapper-->
								<!--begin::Menu item-->
								<div class="d-flex align-items-center" style="column-gap: 80px">
											<div class="menu-item">
												<!--begin::Menu link-->
												<a class="menu-link nav-link py-3 px-4 px-xxl-6" style="font-size: 20px;color: " href="#kt_body" data-kt-scroll-toggle="true" data-kt-drawer-dismiss="true">Beranda</a>
												<!--end::Menu link-->
											</div>
											<!--begin::Menu item-->
											<div class="menu-item">
												<!--begin::Menu link-->
												<a class="menu-link nav-link py-3 px-4 px-xxl-6" style="font-size: 20px;color: " href="#kt_body" data-kt-scroll-toggle="true" data-kt-drawer-dismiss="true">Produk</a>
												<!--end::Menu link-->
											</div>
											<div class="menu-item">
												<!--begin::Menu link-->
												<a class="menu-link nav-link py-3 px-4 px-xxl-6" style="font-size: 20px;color:" href="#kt_body" data-kt-scroll-toggle="true" data-kt-drawer-dismiss="true">Kontak</a>
												<!--end::Menu link-->
											</div>
								</div>
								<!--end::Menu wrapper-->
								<!--begin::Toolbar-->
								<div class="flex-equal text-end ms-1">
									<a href="/login" class="btn btn-success">Sign In</a>
								</div>
								<!--end::Toolbar-->
							</div>
							<!--end::Wrapper-->
						</div>
						<!--end::Container-->
					</div>
					<!--end::Header-->
					<!--begin::Landing hero-->
					<div class="d-flex flex-column flex-center w-100 min-h-350px min-h-lg-500px px-9">
						<!--begin::Heading-->
						<div class="text-center mb-5 mb-lg-10 py-10 py-lg-20">
							<!--begin::Title-->
							<h1 class="text-white lh-base fw-bold fs-2x fs-lg-3x mb-15">Build An Outstanding Solutions 
							<br />with 
							<span style="background: linear-gradient(to right, #12CE5D 0%, #0066ff 100%);-webkit-background-clip: text;-webkit-text-fill-color: transparent;">
								<span id="kt_landing_hero_text">SISI SUPER APPS</span>
							</span></h1>
							<!--end::Title-->
						</div>
						<!--end::Heading-->

					</div>
					<!--end::Landing hero-->
				</div>
				<!--end::Wrapper-->
			</div>
			<!--end::Header Section-->
			<!--begin::How It Works Section-->
			
				<!--begin::Container-->
				<div class="container">
					<!--begin::Heading-->
					<div class="text-center mb-5 mb-lg-10 py-10 py-lg-20">
						<h1 class style=" color: #193164 ; font-size: 40px">Your Transform Business</h1>
						<p class="fs-2" style="color: #193164 ;font-size: 32px">
							Through digital transformation, our solutions will take your business to the next level
						</p>
					</div>
					<!--end::Heading-->
					<!--begin::Row-->
					<div class="row w-100 gy-10 mb-md-20 flex-center" style="column-gap: 40px">
						<!--begin::Col-->
						<div class="row row-cols-1 row-cols-md-3 g-6">
							<div class="col">
								<div class="card" style="background-color: #095384">
								<img src="assets\media\landing\digitalsolution.jpg" class="card-img-top" alt="...">
								<div class="card-body text-center">
									<h1 class="card-title" style="color: white">Digital Solutions</h1>
								</div>
								</div>
							</div>
							<div class="col">
								<div class="card" style="background-color: #095384">
								<img src="assets\media\landing\Shared Service.jpg" class="card-img-top" alt="...">
								<div class="card-body text-center">
									<h1 class="card-title" style="color: white">Shared Service</h1>
								</div>
								</div>
							</div>
							<div class="col">
								<div class="card" style="background-color: #095384">
								<img src="\assets\media\landing\System Integrator.jpg" class="card-img-top" alt="...">
								<div class="card-body text-center">
									<h1 class="card-title" style="color: white">System Integrator</h1>
								</div>
								</div>
							</div>
							</div>
						<!--end::Col-->
						
					</div>
					<!--end::Row-->
					<!--end::container-->
					</div>
				<!--end::How It Works Section-->
					<!--begin::Row-->
							<div class="w-auto p-8" style="background-color: #095384;border-top-left-radius:100px; border-top-right-radius:100px;margin-top: 30px;height:auto;border-bottom-left-radius: px;border-bottom-right-radius: 100px">
								<div class="w-auto p-5 h-200px text-start col-lg-11" >
									<p class="card-title" style="font-display: Panton;font-size: 50px; color: #ffff ; font-weight: bold"  >DIGITAL SULUTIONS</p>
									<div class="row">
										<div class="col-lg-11">
											<p class="fs-3 text-start" style="font-display:Panton;color: #ffff ">Digital solution SISI akan 
											membantu perusahaan anda <br>dalam melakukan transformasi digital melalui layanan<br> end-to-end 
											untuk berbagai industri. </br></br> </p>
										</div>
										<div class="col-lg-1">
											<p class="fs-3 text-end"  style="font-display:Panton"><p onclick="on4()" style="cursor: pointer">Selengkapnya</p></p>	
										</div>
									</div>
								</div>

									<!--begin::Col-->
									<div class="row row-cols-1 row-cols-md-5 g-6 h-auto" >
										<div class="col">
											<div class="card" style="background-color: rgb(255, 255, 255);height:100%">
											<img src="assets\media\landing\forcaerp1.jpg" class="card-img-top" alt="...">
											<div class="card-body ">
												<img class="card-img-rounded w-190px ml-3 h-100px " src="/assets/media/landing/forcaerp.png" alt="" style="margin-top: -80px;margin-bottom: 30px;width: 90%;height: 90%;">
												<p class="card-text" style="font-size: 18px;font-style:inherit;color: #193164">Dengan harapan menjadi platform Information Capital
 													<br>perusahaan, FORCA ERP akan mengintegrasikan proses bisnis 
													dan menyediakan cara yang mudah dan cepat untuk menjalankan bisnis.</p>
											</div>
											</div>
										</div>
										<div class="col">
											<div class="card" style="background-color: rgb(255, 255, 255);height:100%">
												<img src="assets\media\landing\forcapos1.jpg" class="card-img-top" alt="...">
											<div class="card-body ">
												<img class="card-img-rounded w-190px ml-3 h-100px " src="/assets/media/landing/forcapos.png" alt="" style="margin-top: -80px;margin-bottom: 30px;width: 90%;height: 90%;">
												<p class="card-text" style="font-size: 18px;font-style:inherit;color: #193164">Aplikasi Berbasis Web dan android 
													yang digunakan untuk mempermudah dalam mengelola  pencatatan dalam transaksi penjualan,
													dan pembelian </p>
													

											</div>
											</div>
										</div>
										<div class="col">
											<div class="card" style="background-color: rgb(255, 255, 255);height:100%">
											<img src="assets\media\landing\forcacrm1.jpg" class="card-img-top" alt="...">
											<div class="card-body ">
												<img class="card-img-rounded w-190px ml-3 h-100px " src="/assets/media/landing/forcacrm.png" alt="" style="margin-top: -80px;margin-bottom: 30px;width: 90%;height: 90%;">
												<p class="card-text" style="font-size: 18px;font-style:inherit;color: #193164">Layanan aplikasi cloud-based yang digunakan untuk mempertahankan dan membangun 
													loyalitas pelanggan terhadap perusahaan anda dan pembelian.</p>
											</div>
											</div>
										</div>
										<div class="col">
											<div class="card" style="background-color: rgb(255, 255, 255) ;height:100%">
											<img src="assets\media\landing\forcahr1.jpg" class="card-img-top" alt="...">
											<div class="card-body ">
												<img class="card-img-rounded w-190px ml-3 h-100px " src="/assets/media/landing/forcahr.png" alt="" style="margin-top: -80px;margin-bottom: 30px;width: 90%;height: 90%;">
												<p class="card-text" style="font-size: 18px;font-style:inherit;color: #193164">FORCA HR merupakan aplikasi yang akan memudahkan Anda merencanakan, mengelola, dan
													mengevaluasi proses bisnis SDM di perusahaan Anda </p>
												
											</div>
											</div>
										</div>
										<div class="col">
											<div class="card " style="background-color: rgb(255, 255, 255) ;height:100%">
											<img src="assets\media\landing\trukepol.jpg" class="card-img-top" alt="...">
											<div class="card-body ">
												<img class="card-img-rounded w-190px ml-3 h-100px " src="/assets/media/landing/epol.png" alt="" style="margin-top: -80px;margin-bottom: 30px;width: 90%;height: 90%;">
												<p class="card-text" style="font-size: 18px;font-style:inherit;color: #193164">FORCA HR merupakan aplikasi yang akan memudahkan Anda merencanakan, mengelola, dan
													mengevaluasi proses bisnis SDM di perusahaan Anda </p>
												
											</div>
											</div>
										</div>
										
										
									
										</div>
									<!--end::Col-->		
									<div class="w-auto p-8" style="background-color: #ffad49;border-top-left-radius:100px; border-top-right-radius:100px;margin-top: 30px;height:auto; width: 100%;border-bottom-left-radius: 80px;border-bottom-right-radius: 80px">
										
													<div class="w-auto p-5 h-200px text-start col-lg-11" >
														<p class="card-title" style="font-display: Panton;font-size: 50px; color: #ffff ; font-weight: bold"  >DIGITAL SULUTIONS</p>
														<div class="row">
															<div class="col-lg-11">
																<p class="fs-3 text-start" style="font-display:Panton;color: #ffff ">Digital solution SISI akan 
																membantu perusahaan anda <br>dalam melakukan transformasi digital melalui layanan<br> end-to-end 
																untuk berbagai industri. </br></br> </p>
															</div>
															<div class="col-lg-1">
																<p class="fs-3 text-end"  style="font-display:Panton"><a href="#">Selengkapnya</a></p>	
															</div>
														</div>
													</div>

												<!--begin::Col-->
												<div class="row row-cols-1 row-cols-md-5 g-6 h-auto" >
													<div class="col">
														<div class="card" style="background-color: rgb(255, 255, 255);height:100%">
														<img src="assets\media\landing\forcaerp1.jpg" class="card-img-top" alt="...">
														<div class="card-body ">
															<img class="card-img-rounded w-190px ml-3 h-100px " src="/assets/media/landing/forcaerp.png" alt="" style="margin-top: -80px;margin-bottom: 30px;width: 90%;height: 90%;">
															<p class="card-text" style="font-size: 18px;font-style:inherit;color: #193164">Dengan harapan menjadi platform Information Capital
																<br>perusahaan, FORCA ERP akan mengintegrasikan proses bisnis 
																dan menyediakan cara yang mudah dan cepat untuk menjalankan bisnis.</p>
														</div>
														</div>
													</div>
													<div class="col">
														<div class="card" style="background-color: rgb(255, 255, 255);height:100%">
														<img src="assets\media\landing\forcapos1.jpg" class="card-img-top" alt="...">
														<div class="card-body ">
															<img class="card-img-rounded w-190px ml-3 h-100px " src="/assets/media/landing/forcapos.png" alt="" style="margin-top: -80px;margin-bottom: 30px;width: 90%;height: 90%;">
															<p class="card-text" style="font-size: 18px;font-style:inherit;color: #193164">Aplikasi Berbasis Web dan android 
																yang digunakan untuk mempermudah pencatatan dalam transaksi penjualan,
																dan pembelian </p>
																

														</div>
														</div>
													</div>
													<div class="col">
														<div class="card" style="background-color: rgb(255, 255, 255);height:100%">
														<img src="assets\media\landing\forcacrm1.jpg" class="card-img-top" alt="...">
														<div class="card-body ">
															<img class="card-img-rounded w-190px ml-3 h-100px " src="/assets/media/landing/forcacrm.png" alt="" style="margin-top: -80px;margin-bottom: 30px;width: 90%;height: 90%;">
															<p class="card-text" style="font-size: 18px;font-style:inherit;color: #193164">Layanan aplikasi cloud-based yang digunakan untuk mempertahankan dan membangun 
																loyalitas pelanggan terhadap perusahaan anda dan pembelian.</p>
														</div>
														</div>
													</div>
													<div class="col">
														<div class="card" style="background-color: rgb(255, 255, 255) ;height:100%">
														<img src="assets\media\landing\forcahr1.jpg" class="card-img-top" alt="...">
														<div class="card-body ">
															<img class="card-img-rounded w-190px ml-3 h-100px " src="/assets/media/landing/forcahr.png" alt="" style="margin-top: -80px;margin-bottom: 30px;width: 90%;height: 90%;">
															<p class="card-text" style="font-size: 18px;font-style:inherit;color: #193164">FORCA HR merupakan aplikasi yang akan memudahkan Anda merencanakan, mengelola, dan
																mengevaluasi proses bisnis SDM di perusahaan Anda </p>
															
														</div>
														</div>
													</div>
													<div class="col">
														<div class="card " style="background-color: rgb(255, 255, 255) ;height:100%">
														<img src="assets\media\landing\trukepol.jpg" class="card-img-top" alt="...">
														<div class="card-body ">
															<img class="card-img-rounded w-190px ml-3 h-100px " src="/assets/media/landing/epol.png" alt="" style="margin-top: -80px;margin-bottom: 30px;width: 90%;height: 90%;">
															<p class="card-text" style="font-size: 18px;font-style:inherit;color: #193164">FORCA HR merupakan aplikasi yang akan memudahkan Anda merencanakan, mengelola, dan
																mengevaluasi proses bisnis SDM di perusahaan Anda </p>
															
														</div>
														</div>
													</div>
													
							<div class="w-auto p-8" style="background-color: #00A1DA;border-top-left-radius:100px; border-top-right-radius:100px;margin-top: 30px;height:auto;border-bottom-left-radius: 80px;border-bottom-right-radius: 80px">
								<div class="w-auto p-5 h-200px text-start col-lg-11" >
									<p class="card-title" style="font-display: Panton;font-size: 50px; color: #ffff ; font-weight: bold"  >DIGITAL SULUTIONS</p>
									<div class="row">
										<div class="col-lg-11">
											<p class="fs-3 text-start" style="font-display:Panton;color: #ffff ">Digital solution SISI akan 
											membantu perusahaan anda <br>dalam melakukan transformasi digital melalui layanan<br> end-to-end 
											untuk berbagai industri. </br></br> </p>
										</div>
										<div class="col-lg-1">
											<p class="fs-3 text-end"  style="font-display:Panton"><p onclick="on()" style="cursor: pointer">Selengkapnya</p></p>	
										</div>
									</div>
								</div>

									<!--begin::Col-->
									<div class="row row-cols-1 row-cols-md-5 g-6 h-auto" >
										<div class="col">
											<div class="card" style="background-color: rgb(255, 255, 255);height:100%">
											<img src="assets\media\landing\forcaerp1.jpg" class="card-img-top" alt="...">
											<div class="card-body ">
												<img class="card-img-rounded w-190px ml-3 h-100px " src="/assets/media/landing/forcaerp.png" alt="" style="margin-top: -80px;margin-bottom: 30px;width: 90%;height: 90%;">
												<p class="card-text" style="font-size: 18px;font-style:inherit;color: #193164">Dengan harapan menjadi platform Information Capital
 													<br>perusahaan, FORCA ERP akan mengintegrasikan proses bisnis 
													dan menyediakan cara yang mudah dan cepat untuk menjalankan bisnis.</p>
											</div>
											</div>
										</div>
										<div class="col">
											<div class="card" style="background-color: rgb(255, 255, 255);height:100%">
											<img src="assets\media\landing\forcapos1.jpg" class="card-img-top" alt="...">
											<div class="card-body ">
												<img class="card-img-rounded w-190px ml-3 h-100px " src="/assets/media/landing/forcapos.png" alt="" style="margin-top: -80px;margin-bottom: 30px;">
												<p class="card-text" style="font-size: 18px;font-style:inherit;color: #193164">Aplikasi Berbasis Web dan android 
													yang digunakan untuk mempermudah pencatatan dalam transaksi penjualan,
													dan pembelian </p>
													

											</div>
											</div>
										</div>
										<div class="col">
											<div class="card" style="background-color: rgb(255, 255, 255);height:100%">
											<img src="assets\media\landing\forcacrm1.jpg" class="card-img-top" alt="...">
											<div class="card-body ">
												<img class="card-img-rounded w-190px ml-3 h-100px " src="/assets/media/landing/forcacrm.png" alt="" style="margin-top: -80px;margin-bottom: 30px;width: 90%;height: 90%;">
												<p class="card-text" style="font-size: 18px;font-style:inherit;color: #193164">Layanan aplikasi cloud-based yang digunakan untuk mempertahankan dan membangun 
													loyalitas pelanggan terhadap perusahaan anda dan pembelian.</p>
											</div>
											</div>
										</div>
										<div class="col">
											<div class="card" style="background-color: rgb(255, 255, 255) ;height:100%">
											<img src="assets\media\landing\forcahr1.jpg" class="card-img-top" alt="...">
											<div class="card-body ">
												<img class="card-img-rounded w-190px ml-3 h-100px " src="/assets/media/landing/forcahr.png" alt="" style="margin-top: -80px;margin-bottom: 30px;width: 90%;height: 90%;">
												<p class="card-text" style="font-size: 18px;font-style:inherit;color: #193164">FORCA HR merupakan aplikasi yang akan memudahkan Anda merencanakan, mengelola, dan
													mengevaluasi proses bisnis SDM di perusahaan Anda </p>
												
											</div>
											</div>
										</div>
										<div class="col">
											<div class="card " style="background-color: rgb(255, 255, 255) ;height:100%">
											<img src="assets\media\landing\trukepol.jpg" class="card-img-top" alt="...">
											<div class="card-body ">
												<img class="card-img-rounded w-190px ml-3 h-100px " src="/assets/media/landing/epol.png" alt="" style="margin-top: -80px;margin-bottom: 30px;width: 90%;height: 90%;">
												<p class="card-text" style="font-size: 18px;font-style:inherit;color: #193164">FORCA HR merupakan aplikasi yang akan memudahkan Anda merencanakan, mengelola, dan
													mengevaluasi proses bisnis SDM di perusahaan Anda </p>
												
											</div>
											</div>
										</div>
										
										
										
										
									</div>	
							
							</div>	
								</div>
							<!--end::Row-->
				
						</div>			
				</div>
			<!--end::How It Works Section-->
			<!--begin::Custemer Section-->
					<div class=" container" >
						<div class="text-center mb-5 mb-lg-5 py-10 py-lg-20">
							<h1 class style=" color: #193164 ; font-size: 30px">Our Satisfied Clients</h1>
						</div>
						<marquee width="60%" direction="left" height="100px"n style="display: block;" class="mx-auto">
								<img src="assets\media\landing\1-Semen-Indonesia-SIG.png" class=" w-20" alt="..." style="padding-right: 30px;padding-left: 30px">
								<img src="assets\media\landing\2-Solusi-Bangun-Indonesia.png" class=" w-20" alt="..." style="padding-right: 30px;padding-left: 30px">
								<img src="assets\media\landing\3-Bulog.png" class=" w-20" alt="..." style="padding-right: 30px;padding-left: 30px">
								<img src="assets\media\landing\4-GMF.png" class=" w-20" alt="..." style="padding-right: 30px;padding-left: 30px">
								<img src="assets\media\landing\5-Len.png" class=" w-20" alt="..." style="padding-right: 30px;padding-left: 30px">
								<img src="assets\media\landing\6-PTPN-3-holding.png" class=" w-20" alt="..." style="padding-right: 30px;padding-left: 30px">
								<img src="assets\media\landing\7-Semen-Indonesia-Beton.png" class=" w-20" alt="..." style="padding-right: 30px;padding-left: 30px">
								<img src="assets\media\landing\9-PTPN-5.png" class=" w-20" alt="..." style="padding-right: 30px;padding-left: 30px">
								<img src="assets\media\landing\10-Metrocom-Global-Solusi.png" class=" w-20" alt="..." style="padding-right: 30px;padding-left: 30px">
								<img src="assets\media\landing\11-Pupuk-Indonesia.png" class=" w-20" alt="..." style="padding-right: 30px;padding-left: 30px">
								<img src="assets\media\landing\12-Semen-Indonesia-International.png" class=" w-20" alt="..." style="padding-right: 30px;padding-left: 30px">
								<img src="assets\media\landing\13-Sinar-Sukses-Mandiri.png" class=" w-20" alt="..." style="padding-right: 30px;padding-left: 30px">
								<img src="assets\media\landing\14-Bima-Sepaja-Abadi.png" class=" w-20" alt="..." style="padding-right: 30px;padding-left: 30px">
								
						</marquee>
						<marquee width="60%"  direction="left" height="100px"n style="display: block;" class="mx-auto">
									<img src="assets\media\landing\35-PT-PPI.png" class=" w-20" alt="..." style="padding-right: 30px;padding-left: 30px">
									<img src="assets\media\landing\15-Waskita-Karya.png" class=" w-20" alt="..." style="padding-right: 30px;padding-left: 30px">
									<img src="assets\media\landing\16-PT-Petrosida-Gresik.png" class=" w-20" alt="..." style="padding-right: 30px;padding-left: 30px">
									<img src="assets\media\landing\17-Semen-Padang.png" class=" w-20" alt="..." style="padding-right: 30px;padding-left: 30px">
									<img src="assets\media\landing\19-Semen-Gresik.png" class=" w-20" alt="..." style="padding-right: 30px;padding-left: 15px">
									<img src="assets\media\landing\20-Bank-Syariah-Mandiri.png" class=" w-20" alt="..." style="padding-right: 20px;padding-left: 30px">
									<img src="assets\media\landing\21-Igasar.png" class=" w-20" alt="..." style="padding-right: 30px;padding-left: 10px">
									<img src="assets\media\landing\22-Semen-Baturaja.png" class=" w-20" alt="..." style="padding-right: 30px;padding-left: 30px">
									<img src="assets\media\landing\23-PTPN-10.png" class=" w-20" alt="..." style="padding-right: 30px;padding-left: 30px">
									<img src="assets\media\landing\24-Damri.png" class=" w-20" alt="..." style="padding-right: 30px;padding-left: 30px">
									<img src="assets\media\landing\30-Bank-Jatim-BPD-Jatim.png" class=" w-20" alt="..." style="padding-right: 30px;padding-left: 30px">
									<img src="assets\media\landing\31-Bio-Farma.png" class=" w-20" alt="..." style="padding-right: 30px;padding-left: 30px">
									<img src="assets\media\landing\32-ILUNI-UI.png" class=" w-20" alt="..." style="padding-right: 30px;padding-left: 30px">
									<img src="assets\media\landing\33-PT-PII.png" class=" w-20" alt="..." style="padding-right: 30px;padding-left: 30px">
									
						</marquee>
					
					</div>
					
			
				
							<!--end::Row-->
							
			<!--end::Costumer Section-->
			<!--begin::News Section-->
						<div class="container">
							<div class="text-center mb-9 mb-lg-5 py-10 py-lg-20">
								<h1 class style=" color: #193164 ; font-size: 30px">Discover SISI SUPER APPS</h1>
								<p class="fs-6">Find out all the latest news from the SISI SUPER APPS.</p>
							</div>

							
								<div class="row row-cols-1 row-cols-md-3 g-4">
									<div class="col">
										<div class="card shadow p-3 mb-5 bg-body rounded" style="background-color: rgb(255, 255, 255) ;height:100%">
											<img src="assets\media\landing\forcacrm1.jpg" class="card-img-top" alt="...">
											<div class="card-body ">
												<img class="card-img-rounded w-100px ml-3 h-100px " src="/assets/media/landing/forcacrm.png" alt="" style="margin-top: -80px;margin-bottom: 30px;width: 90%;height: 90%;">
												<p class="card-text " style="font-size: 18px;font-style:inherit;color: #193164  ">Layanan aplikasi cloud-based yang digunakan untuk mempertahankan dan membangun 
													loyalitas pelanggan terhadap perusahaan anda dan pembelian.</p>
											</div>
											</div>
										</div>
										<div class="col">
											<div class="card shadow p-3 mb-5 bg-body rounded" style="background-color: rgb(255, 255, 255) ;height:100%">
											<img src="assets\media\landing\forcahr1.jpg" class="card-img-top" alt="...">
											<div class="card-body ">
												<img class="card-img-rounded w-190px ml-3 h-100px " src="/assets/media/landing/forcahr.png" alt="" style="margin-top: -80px;margin-bottom: 30px;width: 90%;height: 90%;">
												<p class="card-text" style="font-size: 18px;font-style:inherit;color: #193164">FORCA HR merupakan aplikasi yang akan memudahkan Anda merencanakan, mengelola, dan
													mengevaluasi proses bisnis SDM di perusahaan Anda </p>
												
											</div>
											</div>
										</div>
										<div class="col">
											<div class="card shadow p-3 mb-5 bg-body rounded" style="background-color: rgb(255, 255, 255) ;height:100%">
											<img src="assets\media\landing\trukepol.jpg" class="card-img-top" alt="...">
											<div class="card-body ">
												<img class="card-img-rounded w-190px ml-3 h-100px " src="/assets/media/landing/epol.png" alt="" style="margin-top: -80px;margin-bottom: 30px;width: 90%;height: 90%;">
												<p class="card-text" style="font-size: 18px;font-style:inherit;color: #193164">FORCA HR merupakan aplikasi yang akan memudahkan Anda merencanakan, mengelola, dan
													mengevaluasi proses bisnis SDM di perusahaan Anda </p>
												
											</div>
											</div>
										</div>
									
								</div>
							
							
							
							
							
							
						</div>
			
			<!--end::News Section-->
		
			<!--begin::Testimonials Section-->
			<style>
				#overlay4 {
				position: fixed;
				display: none;
				width: 100%;
				height: 100%;
				top: 0;
				left: 0;
				right: 0;
				bottom: 0;
				background-color: rgba(16, 16, 17, 0.5);
				z-index: 2;
			
				
				}

				#text4{
				position: absolute;
				top: 50%;
				left: 50%;
				font-size: 50px;
				color: white;
				transform: translate(-50%,-50%);
				-ms-transform: translate(-50%,-50%);
				}
			</style>

				<body>
					<div id="overlay4" >
						<div class="w-auto p-8 animate__animated animate__backInUp" style="background-color: #095384;border-top-left-radius:100px; border-top-right-radius:100px;margin-top: 100px;height:auto;border-bottom-left-radius:100px;border-bottom-right-radius: 100px">
								<div class="w-auto p-5 h-200px text-start col-lg-11" >
									<p class="card-title" style="font-display: Panton;font-size: 50px; color: #ffff ; font-weight: bold"  >DIGITAL SULUTIONS</p>
									<div class="row">
										<div class="col-lg-11">
											<p class="fs-3 text-start" style="font-display:Panton;color: #ffff ">Digital solution SISI akan 
											membantu perusahaan anda <br>dalam melakukan transformasi digital melalui layanan<br> end-to-end 
											untuk berbagai industri. </br></br> </p>
										</div>
										<div class="col-lg-1">
											<p class="fs-3 text-end"  style="font-display:Panton ;"><p onclick="off4()" style="cursor: pointer"><i class="fa-solid fa-xmark " style="font-size: 25px;color: #ffff"></i></p></p>	
										</div>
									</div>
								</div>
								<style>
									.swiper {
									width: 80%;
									height: 80%;
								}

								.swiper-slide {
									text-align:left;
									font-size: 18px;
									background: #fff;
									border-radius: 10px;

									/* Center slide text vertically */
									display: -webkit-box;
									display: -ms-flexbox;
									display: -webkit-flex;
									display: flex;
									-webkit-box-pack: center;
									-ms-flex-pack: center;
									-webkit-justify-content: center;
									justify-content: center;
									-webkit-box-align: center;
									-ms-flex-align: center;
									-webkit-align-items: center;
									align-items: center;
								}
								</style>

								<body>
								<!-- Swiper -->
								<div class="swiper mySwiper">
									<div class="swiper-wrapper">

										<div class="swiper-slide">
											<div class="card" style="background-color: rgb(255, 255, 255);height:100%">
											<img src="assets\media\landing\forcaerp1.jpg" class="card-img-top" alt="...">
											<div class="card-body ">
												<img class="card-img-rounded w-190px ml-3 h-100px " src="/assets/media/landing/forcaerp.png" alt="" style="margin-top: -80px;margin-bottom: 30px;width: 90%;height: 90%;">
												<p class="card-text" style="font-size: 18px;font-style:inherit;color: #193164">Dengan harapan menjadi platform Information Capital
 													<br>perusahaan, FORCA ERP akan mengintegrasikan proses bisnis 
													dan menyediakan cara yang mudah dan cepat untuk menjalankan bisnis.</p>
											</div>
												<p class="fs-3 text-end p-4"  style="font-display:Panton">
													<a href="https://sisi.id/solutions/digital-solution/forca-erp/">Detail</a>
												</p>
											</div>
										</div>
										<div class="swiper-slide" style="height: auto">
											<div class="card" style="background-color: rgb(255, 255, 255);height:100%">
											<img src="assets\media\landing\forcapos1.jpg" class="card-img-top" alt="...">
											<div class="card-body w-20 h-auto">
														<img class="card-img-rounded w-190px ml-3 h-100px " src="/assets/media/landing/forcapos.png" alt="" style="margin-top: -80px;margin-bottom: 30px;width: 90%;height: 90%;">
														<p class="card-text" style="font-size: 18px;font-style:inherit;color: #193164">Aplikasi Berbasis Web dan android 
														yang digunakan untuk mempermudah mengelola pencatatan dalam transaksi penjualan dan pembelian.         
														</p>
												</div>
												<p class="fs-3 text-end p-4"  style="font-display:Panton">
													<a href="https://sisi.id/solutions/digital-solution/forca-pos/">Detail</a>
												</p>
											</div>
												
										</div>
										<div class="swiper-slide">
											<div class="card" style="background-color: rgb(255, 255, 255);height:100%">
											<img src="assets\media\landing\forcacrm1.jpg" class="card-img-top" alt="...">
											<div class="card-body ">
												<img class="card-img-rounded w-190px ml-3 h-100px " src="/assets/media/landing/forcacrm.png" alt="" style="margin-top: -80px;margin-bottom: 30px;width: 90%;height: 90%;">
												<p class="card-text" style="font-size: 18px;font-style:inherit;color: #193164">Layanan aplikasi cloud-based yang digunakan untuk mempertahankan dan membangun 
													loyalitas pelanggan terhadap perusahaan anda dan pembelian.</p>
											</div>
											<p class="fs-3 text-end p-4"  style="font-display:Panton">
													<a href="https://sisi.id/solutions/digital-solution/forca-crm/">Detail</a>
												</p>
											</div>	
										</div>
										<div class="swiper-slide">
											<div class="card" style="background-color: rgb(255, 255, 255) ;height:100%">
											<img src="assets\media\landing\forcahr1.jpg" class="card-img-top" alt="...">
											<div class="card-body ">
												<img class="card-img-rounded w-190px ml-3 h-100px " src="/assets/media/landing/forcahr.png" alt="" style="margin-top: -80px;margin-bottom: 30px;width: 90%;height: 90%;">
												<p class="card-text" style="font-size: 18px;font-style:inherit;color: #193164">FORCA HR merupakan aplikasi yang akan memudahkan Anda merencanakan, mengelola, dan
													mengevaluasi proses bisnis SDM di perusahaan Anda </p>
												
											</div>
												<p class="fs-3 text-end p-4"  style="font-display:Panton">
													<a href="https://sisi.id/solutions/digital-solution/forca-hr/">Detail</a>
												</p>
											</div>
											
										</div>
										<div class="swiper-slide">
											<div class="card " style="background-color: rgb(255, 255, 255) ;height:100%">
												<img src="assets\media\landing\trukepol.jpg" class="card-img-top" alt="...">
												<div class="card-body ">
													<img class="card-img-rounded w-190px ml-3 h-100px " src="/assets/media/landing/epol.png" alt="" style="margin-top: -80px;margin-bottom: 30px;width: 90%;height: 90%;">
													<p class="card-text" style="font-size: 18px;font-style:inherit;color: #193164">EPOOOL merupakan Transportation Management System yang akan memudahkan Anda dalam mengelola logistik perusahaan dengan lebih efektif dan efisien.</p>

												</div>
												<p class="fs-3 text-end p-4"  style="font-display:Panton">
													<a href="https://sisi.id/solutions/digital-solution/epoool/">Detail</a>
												</p>
											</div>
										</div>
										<div class="swiper-slide">
											<div class="card " style="background-color: rgb(255, 255, 255) ;height:100%">
												<img src="assets\media\landing\forcaess.jpg" class="card-img-top" alt="...">
												<div class="card-body ">
													<img class="card-img-rounded w-190px ml-3 h-100px" src="/assets/media/landing/forcaess1cc.png" alt="" style="margin-top: -80px;margin-bottom: 30px;width: 90%;height: 90%;">
													<p class="card-text" style="font-size: 18px;font-style:inherit;color: #193164">FORCA ESS merupakan layanan terpisah dari FORCA HR yang dapat memudahkan bisnis anda dalam memantau kinerja dan kedisiplinan karyawan agar lebih produktif.</p>

												</div>
												<p class="fs-3 text-end p-4"  style="font-display:Panton">
													<a href="https://sisi.id/solutions/digital-solution/forca-ess/">Detail</a>
												</p>
											</div>
										</div>
										<div class="swiper-slide">
											<div class="card " style="background-color: rgb(255, 255, 255) ;height:100%">
												<img src="assets\media\landing\aksestoko.png" class="card-img-top" alt="...">
												<div class="card-body ">
													<img class="card-img-rounded w-190px ml-3 h-100px " src="assets\media\landing\aksestoko (2).png" alt="" style="margin-top: -80px;margin-bottom: 30px;width: 90%;height: 90%;">
													<p class="card-text" style="font-size: 18px;font-style:inherit;color: #193164">EPOOOL merupakan Transportation Management System yang akan memudahkan Anda dalam mengelola logistik perusahaan dengan lebih efektif dan efisien.</p>

												</div>
												<p class="fs-3 text-end p-4"  style="font-display:Panton">
													<a href="">Detail</a>
												</p>
											</div>
										</div>
									</div>
									<div class="swiper-button-next"></div>
									<div class="swiper-button-prev"></div>
									<div class="swiper-pagination"></div>
								</div>

								<!-- Swiper JS -->
								<script src="https://cdn.jsdelivr.net/npm/swiper/swiper-bundle.min.js"></script>

								<!-- Initialize Swiper -->
								<script>
								var swiper = new Swiper('.swiper', {
								slidesPerView: 4,
								direction: getDirection(),
								spaceBetween: 30,
								loop: true,
								slidesPerGroup: 4,

								navigation: {
									nextEl: '.swiper-button-next',
									prevEl: '.swiper-button-prev',
								},
								on: {
									resize: function () {
									swiper.changeDirection(getDirection());
									},
								},
								});

								function getDirection() {
								var windowWidth = window.innerWidth;
								var direction = window.innerWidth <= 760 ? 'vertical' : 'horizontal';

								return direction;
								}
							</script>
							</body>
						</div>							
					</div>


					<script>
					function on4() {
					document.getElementById("overlay4").style.display = "block";
					document.createElement("").style.display = "block";
					}

					function off4() {
					document.getElementById("overlay4").style.display = "none";
					}
					</script>
							


				</div>

				<!--begin::Footer Section-->
			<div class="mb-0">
				<!--begin::Curve top-->
				<div class="landing-curve landing-dark-color">
					<svg viewBox="15 -1 1470 48" fill="none" xmlns="http://www.w3.org/2000/svg">
						<path d="M1 48C4.93573 47.6644 8.85984 47.3311 12.7725 47H1489.16C1493.1 47.3311 1497.04 47.6644 1501 48V47H1489.16C914.668 -1.34764 587.282 -1.61174 12.7725 47H1V48Z" fill="currentColor"></path>
					</svg>
				</div>
				<!--end::Curve top-->
				<!--begin::Wrapper-->
				<div class="landing-dark-bg pt-20">
					<!--begin::Container-->
					<div class="container">
						<!--begin::Row-->
						<div class="row py-10 py-lg-20">
							<!--begin::Col-->
							<div class="col-lg-6 pe-lg-16 mb-10 mb-lg-0">
								<!--begin::Block-->
								<div class="rounded landing-dark-border p-9 mb-10">
									<!--begin::Title-->
									<h2 class="text-white">Would you need a Custom License?</h2>
									<!--end::Title-->
									<!--begin::Text-->
									<span class="fw-normal fs-4 text-gray-700">Email us to 
									<a href="https://keenthemes.com/support" class="text-white opacity-50 text-hover-primary">support@keenthemes.com</a></span>
									<!--end::Text-->
								</div>
								<!--end::Block-->
								<!--begin::Block-->
								<div class="rounded landing-dark-border p-9">
									<!--begin::Title-->
									<h2 class="text-white">How About a Custom Project?</h2>
									<!--end::Title-->
									<!--begin::Text-->
									<span class="fw-normal fs-4 text-gray-700">Use Our Custom Development Service. 
									<a href="pages/user-profile/overview.html" class="text-white opacity-50 text-hover-primary">Click to Get a Quote</a></span>
									<!--end::Text-->
								</div>
								<!--end::Block-->
							</div>
							<!--end::Col-->
							<!--begin::Col-->
							<div class="col-lg-6 ps-lg-16">
								<!--begin::Navs-->
								<div class="d-flex justify-content-center">
									<!--begin::Links-->
									<div class="d-flex fw-semibold flex-column me-20">
										<!--begin::Subtitle-->
										<h4 class="fw-bold text-gray-400 mb-6">More for Metronic</h4>
										<!--end::Subtitle-->
										<!--begin::Link-->
										<a href="https://keenthemes.com/faqs" class="text-white opacity-50 text-hover-primary fs-5 mb-6">FAQ</a>
										<!--end::Link-->
										<!--begin::Link-->
										<a href="documentation/getting-started.html" class="text-white opacity-50 text-hover-primary fs-5 mb-6">Documentaions</a>
										<!--end::Link-->
										<!--begin::Link-->
										<a href="https://www.youtube.com/c/KeenThemesTuts/videos" class="text-white opacity-50 text-hover-primary fs-5 mb-6">Video Tuts</a>
										<!--end::Link-->
										<!--begin::Link-->
										<a href="documentation/getting-started/changelog.html" class="text-white opacity-50 text-hover-primary fs-5 mb-6">Changelog</a>
										<!--end::Link-->
										<!--begin::Link-->
										<a href="https://devs.keenthemes.com/" class="text-white opacity-50 text-hover-primary fs-5 mb-6">Support Forum</a>
										<!--end::Link-->
										<!--begin::Link-->
										<a href="https://keenthemes.com/blog" class="text-white opacity-50 text-hover-primary fs-5">Blog</a>
										<!--end::Link-->
									</div>
									<!--end::Links-->
									<!--begin::Links-->
									<div class="d-flex fw-semibold flex-column ms-lg-20">
										<!--begin::Subtitle-->
										<h4 class="fw-bold text-gray-400 mb-6">Stay Connected</h4>
										<!--end::Subtitle-->
										<!--begin::Link-->
										<a href="https://www.facebook.com/keenthemes" class="mb-6">
											<img src="assets/media/svg/brand-logos/facebook-4.svg" class="h-20px me-2" alt="" />
											<span class="text-white opacity-50 text-hover-primary fs-5 mb-6">Facebook</span>
										</a>
										<!--end::Link-->
										<!--begin::Link-->
										<a href="https://github.com/KeenthemesHub" class="mb-6">
											<img src="assets/media/svg/brand-logos/github.svg" class="h-20px me-2" alt="" />
											<span class="text-white opacity-50 text-hover-primary fs-5 mb-6">Github</span>
										</a>
										<!--end::Link-->
										<!--begin::Link-->
										<a href="https://twitter.com/keenthemes" class="mb-6">
											<img src="assets/media/svg/brand-logos/twitter.svg" class="h-20px me-2" alt="" />
											<span class="text-white opacity-50 text-hover-primary fs-5 mb-6">Twitter</span>
										</a>
										<!--end::Link-->
										<!--begin::Link-->
										<a href="https://dribbble.com/keenthemes" class="mb-6">
											<img src="assets/media/svg/brand-logos/dribbble-icon-1.svg" class="h-20px me-2" alt="" />
											<span class="text-white opacity-50 text-hover-primary fs-5 mb-6">Dribbble</span>
										</a>
										<!--end::Link-->
										<!--begin::Link-->
										<a href="https://www.instagram.com/keenthemes" class="mb-6">
											<img src="assets/media/svg/brand-logos/instagram-2-1.svg" class="h-20px me-2" alt="" />
											<span class="text-white opacity-50 text-hover-primary fs-5 mb-6">Instagram</span>
										</a>
										<!--end::Link-->
									</div>
									<!--end::Links-->
								</div>
								<!--end::Navs-->
							</div>
							<!--end::Col-->
						</div>
						<!--end::Row-->
					</div>
					<!--end::Container-->
					<!--begin::Separator-->
					<div class="landing-dark-separator"></div>
					<!--end::Separator-->
					<!--begin::Container-->
					<div class="container">
						<!--begin::Wrapper-->
						<div class="d-flex flex-column flex-md-row flex-stack py-7 py-lg-10">
							<!--begin::Copyright-->
							<div class="d-flex align-items-center order-2 order-md-1">
								<!--begin::Logo-->
								<a href="landing.html">
									<img alt="Logo" src="assets/media/logos/landing.svg" class="h-15px h-md-20px" />
								</a>
								<!--end::Logo image-->
								<!--begin::Logo image-->
								<span class="mx-5 fs-6 fw-semibold text-gray-600 pt-1" href="https://keenthemes.com/">© 2022 Keenthemes Inc.</span>
								<!--end::Logo image-->
							</div>
							<!--end::Copyright-->
							<!--begin::Menu-->
							<ul class="menu menu-gray-600 menu-hover-primary fw-semibold fs-6 fs-md-5 order-1 mb-5 mb-md-0">
								<li class="menu-item">
									<a href="https://keenthemes.com/" target="_blank" class="menu-link px-2">About</a>
								</li>
								<li class="menu-item mx-5">
									<a href="https://devs.keenthemes.com/" target="_blank" class="menu-link px-2">Support</a>
								</li>
								<li class="menu-item">
									<a href="#" target="_blank" class="menu-link px-2">Purchase</a>
								</li>
							</ul>
							<!--end::Menu-->
						</div>
						<!--end::Wrapper-->
					</div>
					<!--end::Container-->
				</div>
				<!--end::Wrapper-->
			</div>
			<!--end::Footer Section-->

   



		<!--begin::Javascript-->
		<script>var hostUrl = "assets/index.html";</script>
		<!--begin::Global Javascript Bundle(used by all pages)-->
		<script src="assets/plugins/global/plugins.bundle.js"></script>
		<script src="assets/js/scripts.bundle.js"></script>
		<!--end::Global Javascript Bundle-->
		<!--begin::Vendors Javascript(used by this page)-->
		<script src="assets/plugins/custom/fslightbox/fslightbox.bundle.js"></script>
		<script src="assets/plugins/custom/typedjs/typedjs.bundle.js"></script>
		<!--end::Vendors Javascript-->
		<!--begin::Custom Javascript(used by this page)-->
		<script src="assets/js/custom/landing.js"></script>
		<script src="assets/js/custom/pages/pricing/general.js"></script>
		<!--end::Custom Javascript-->
		<script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
		<!--end::Javascript-->
		
		<script src="https://cdn.jsdelivr.net/npm/swiper/swiper-bundle.min.js"></script>
		
	</body>
	<!--end::Body-->

<!-- Mirrored from preview.keenthemes.com/metronic8/demo1/landing.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 08 Jul 2022 06:29:41 GMT -->
</html>