<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\MenuController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\JenisUserController;
use App\Http\Controllers\LoginController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('landing');
});

Route::get('/login', function () {
    return view('login');
});

Route::get('/login', [LoginController::class, 'index']);
Route::post('/login', [LoginController::class, 'store']);
Route::get('/logout', [LoginController::class, 'destroy']);

Route::group(['middleware' => ['auth']], function(){
        Route::get('/default','App\Http\Controllers\DashboardController@index');
        
        // profile
        Route::get('/profile','App\Http\Controllers\ProfileController@index');
        Route::post('/profile/updatefoto','App\Http\Controllers\ProfileController@updateFoto');
        Route::post('/profile/updateprofile','App\Http\Controllers\ProfileController@updateProfile');


        Route::get('/menu', [MenuController::class, 'index']);
        Route::get('/menu/list', [MenuController::class, 'list']);

        //menu 
        Route::resource('menu','App\Http\Controllers\MenuController')->except(['show','update']);
        Route::get('/menu/list', 'App\Http\Controllers\MenuController@list');
        Route::post('/menu/submenu', 'App\Http\Controllers\MenuController@submenu');
        Route::get('/menu/{id}/show', 'App\Http\Controllers\MenuController@show');


        //user
        Route::resource('user','App\Http\Controllers\UserController')->except(['show']);
        Route::get('/user/list', 'App\Http\Controllers\UserController@list');
        Route::post('/user/check', 'App\Http\Controllers\UserController@check');
        Route::post('/user/store', 'App\Http\Controllers\UserController@store');
        Route::post('/user/update/{id}', 'App\Http\Controllers\UserController@update');
        Route::get('/user/{id}/listmenu', 'App\Http\Controllers\UserController@listMenu');
        Route::post('/user/storemenu', 'App\Http\Controllers\UserController@storeMenu');
        Route::post('/user/changepassword/{id}', 'App\Http\Controllers\UserController@changePassword');

        //jenis user
        Route::resource('jenisuser','App\Http\Controllers\JenisUserController')->except(['show','update']); 
        Route::get('/jenisuser/list', 'App\Http\Controllers\JenisUserController@list');
        Route::get('/jenisUser/{id}/listmenu', 'App\Http\Controllers\JenisUserController@listMenu');
        // Route::get('/jenisuser/listmenu/{id}', 'App\Http\Controllers\JenisUserController@listMenu');
        Route::post('/jenisuser/storemenu', 'App\Http\Controllers\JenisUserController@storeMenu');

        Route::resource('pegawai','App\Http\Controllers\PegawaiController')->except(['show','update']);

        //jenis user
        Route::get('/jenis-user', [JenisUserController::class, 'index']);

        //User Activity
        Route::resource('useractivity','App\Http\Controllers\UserActivityController')->except(['show','update']); 
        Route::get('/useractivity/list', 'App\Http\Controllers\UserActivityController@list');

        //Error Application
        Route::resource('errorapplication','App\Http\Controllers\ErrorApplicationController')->except(['show','update']); 
        Route::get('/errorapplication/list', 'App\Http\Controllers\ErrorApplicationController@list');
});