<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DB;

class UserFoto extends Model
{

   public function user(){
      //user foto hanya punya 1 user 
      return $this->belongsTo(User::class);
   } 

   protected $table = 'user_foto';
   protected $fillable = [
    'id',
    'id_user',
    'no_dokumen',
    'create_by',
    'dalete_mark',
    'update_by',
   ];


}
