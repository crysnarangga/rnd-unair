<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DB;
class MenuUser extends Model
{
    protected $table = 'menu_user';
    protected $fillable = [
        'no_seting',
        'id_user',
        'menu_id',
        'create_date',
        'create_time',
        'dalete_mark',
        'update_by',
        'update_date',
       
    ];
}
