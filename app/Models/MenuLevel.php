<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DB;
class MenuLevel extends Model
{ protected $table = 'menu_level';
    protected $fillable = [
        'id_level',
        'level',
    ];
}
