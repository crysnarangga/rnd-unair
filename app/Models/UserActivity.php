<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DB;
class UserActivity extends Model
{
    protected $table = 'user_activity';
    protected $fillable = [
        'no_activity',
        'id_user',
        'discripsi',
        'status',
        'menu_id',
        'delete',
        'create_by',
        'create_date',
    ];
}
