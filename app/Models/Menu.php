<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    protected $table = 'menu';
    
    protected $guarded = [];

    public function menuChilds()
    {
        return $this->hasMany("App\Models\Menu", "parent_id", "id");
    }
}
