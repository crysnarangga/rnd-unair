<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DB;
class ErrorApplication extends Model
{
    protected $table = 'i_error_application';
    protected $fillable = [
        'error_id',
        'id_user',
        'modules',
        'controller',
        'function',
        'error_line',
        'error_message',
        'status',
        'param',
        'create_date',
        'create_time',
        'delete_mark',
        'update_by',
        'update_date',
       
    ];
}

