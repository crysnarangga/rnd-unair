<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DB;
class JenisUser extends Model
{
    protected $table = 'jenis_user';
    protected $fillable = [
        'id',
        'jenis_user',
       
    ];
}
