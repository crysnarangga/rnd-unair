<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DB;
class SettingMenu extends Model
{
    protected $table = 'setting_menu_user';
    protected $fillable = [
           'id_jenis_user',
            'menu_id',
            'create_by',
            'dalete_mark',
            'update_by',
    ];
}
