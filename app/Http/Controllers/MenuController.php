<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Menu;
use App\Models\MenuLevel;
use App\Models\MenuUser;
use App\Models\UserFoto;
use App\Models\UserActivity;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Lcobucci\JWT\Token;
use PhpParser\Node\Stmt\TryCatch;

class MenuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $data['dashboard_menu'] = $this->getDashboardMenu();
            $data['menu'] = Menu::where('id_level', '1')->get();
            $foto = UserFoto::where('id_user','=', Auth::user()->id)->get();
            $foto_user = $foto[0]->no_dokumen;
            $data['level'] = MenuLevel::get();

            UserActivity::create([
                'id_user' => Auth::user()->id,
                'discripsi' => 'Melihat Menu',
                'status' => 'Success',
                'menu_id' => 'MEN',
                'create_by' => Auth::user()->id,
                'delete' => '0',
            ]);
            // return 'test';
            return view('menu.menu', $data)->with('foto', $foto_user);

        } catch (\Exception $e) {
            UserActivity::create([
                'id_user' => Auth::user()->id,
                'discripsi' => 'Melihat Menu',
                'status' => 'Failed',
                'menu_id' => 'MEN',
                'create_by' => Auth::user()->id,
                'delete' => '0'
            ]);
            return response()->json(['error' => $e->getMessage()], 500);
        }
        
    }

    public function list(Request $request)
    {
        $menu = Menu::leftJoin('menu_level as b', 'b.id', '=', 'menu.id_level')
        ->leftJoin('menu as c', 'c.id', '=', 'menu.parent_id')
        ->select('menu.*', 'b.level', 'c.menu_name as parent_name', 'menu.id as id_menu')
        ->where('menu.delete_mark','=', '0')->get();
        if($request->ajax()){
            return datatables()->of($menu)
                        ->addColumn('action', function($data){
                            $button = 
                            '<div class= "btn-group dropend ">
                            <button type="button" class="btn btn-primary dropdown-toggle" data-bs-toggle="dropdown" aria-expanded="false" data-bs-auto-close="true" aria-haspopup="true">
                             <i class="fa fa-bars"></i>
                            </button>
                            <ul class="dropdown-menu" style="background-color: white;">
                              <li><a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$data->id_menu.'" data-original-title="Edit" class="edit edit-post dropdown-item" style="color: #FFA500;"><i class="far fa-edit" style="color: #FFA500;"></i> Edit</a></li>
                              <li><button type="button" name="delete" id="'.$data->id_menu.'" class="delete btn-sm dropdown-item" style="color: red;"><i class="far fa-trash-alt" style="color: red;"></i> Delete</button></li>
                            </ul>
                          </div>';
                            return $button;
                        })
                        ->rawColumns(['action'])
                        ->addIndexColumn()
                        ->make(true);
        }    
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $id = $request->id;
            $data = Menu::where('parent_id', '=', $id)->where('id_level', '=', '2')->get();
            $post   =   Menu::updateOrCreate(['id' => $id],
                    [
                        'menu_name' => $request->menu_name,
                        'menu_link' => $request->menu_link,
                        'menu_icon' => $request->menu_icon,
                        'delete_mark' => '0',
                        'create_by' => Auth::user()->id,
                        'update_by' => Auth::user()->id,
                        'id_level' => $request->id_level,
                        'parent_id' => $request->parent,
                    ]);
        
            if($post){
                if($request->id_level == '2'){
                    $post = DB::table('menu')->where('parent_id', '=', $id)->where('id_level', '=', '2')->update(['parent_id' => null, 'id_level' => '1']);
                }
            }
            UserActivity::create([
                'id_user' => Auth::user()->id,
                'discripsi' => 'Membuat / Mengupdate Menu',
                'status' => 'Success',
                'menu_id' => 'MEN',
                'create_by' => Auth::user()->id,
                'delete' => '0'
            ]);
            return response()->json($post);
        } catch (\Exception $e) {
            UserActivity::create([
                'id_user' => Auth::user()->id,
                'discripsi' => 'Membuat / Mengupdate Menu',
                'status' => 'Failed',
                'menu_id' => 'MEN',
                'create_by' => Auth::user()->id,
                'delete' => '0'
            ]);
            return response()->json(['error' => $e->getMessage()], 500);
        }
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try{
            $where = array('id' => $id);
            $post  = Menu::where($where)->first();

            UserActivity::create([
                'id_user' => Auth::user()->id,
                'discripsi' => 'Mengambil Data Menu',
                'status' => 'Success',
                'menu_id' => 'MEN',
                'create_by' => Auth::user()->id,
                'delete' => '0',
            ]);
        
            return response()->json($post);

        }catch(\Exception $e){
            UserActivity::create([
                'id_user' => Auth::user()->id,
                'discripsi' => 'Mengambil Data Menu',
                'status' => 'Failed',
                'menu_id' => 'MEN',
                'create_by' => Auth::user()->id,
                'delete' => '0',
            ]);

            return response()->json(['error' => $e->getMessage()], 500);
        }
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            $post = Menu::where('id',$id)->update(['delete_mark' => '1']);
            $post = DB::table('menu')->where('parent_id', '=', $id)->where('id_level', '=', '2')->update(['parent_id' => null, 'id_level' => '1']);
    
            
            UserActivity::create([
                'id_user' => Auth::user()->id,
                'discripsi' => 'Menghapus Data Menu',
                'status' => 'Success',
                'menu_id' => 'MEN',
                'create_by' => Auth::user()->id,
                'delete' => '0',
            ]);
            
            return response()->json($post);
        } catch (\Exception $e) {
            UserActivity::create([
                'id_user' => Auth::user()->id,
                'discripsi' => 'Menghapus Data Menu',
                'status' => 'Failed',
                'menu_id' => 'MEN',
                'create_by' => Auth::user()->id,
                'delete' => '0',
            ]);
            return response()->json(['error' => $e->getMessage()], 500);
        }
    }
}
