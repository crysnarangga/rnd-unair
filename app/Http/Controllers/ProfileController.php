<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\UserFoto;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProfileController extends Controller
{
    public function index()
    {
        $data['dashboard_menu'] = $this->getDashboardMenu();
        $foto = UserFoto::where('id_user','=', Auth::user()->id)->get();
        $foto_user = $foto[0]->no_dokumen;
        return view('dashboard.profile', $data)->with('foto', $foto_user);
    }

    public function updateFoto(Request $request)
    {
        $file= $request->file('photo');
        $filename= date('YmdHi').$file->getClientOriginalName();
        $file-> move(public_path('assets/media/img/profile'), $filename);
        $post = UserFoto::where('id_user', Auth::user()->id)->update(
            [
                'no_dokumen' => $filename,
                'update_by'=> Auth::user()->id,
            ]);
        if($post){
            return redirect('/profile')->with('success', 'Foto berhasil diubah');
        }else{
            return redirect('/profile')->with('error', 'Foto gagal diubah');
        }
    }

    public function updateProfile(Request $request)
    {
        $post = User::where('id', Auth::user()->id)->update(
            [
                'username' => $request->username,
                'nama_user' => $request->nama_user,
                'email' => $request->email,
                'no_hp' => $request->no_hp,
                'wa' => $request->wa,
                'pin' => $request->pin,
                'update_by'=> Auth::user()->id,
            ]);
        if($post){
            return redirect('/profile')->with('success', 'Profile berhasil diubah');
        }else{
            return redirect('/profile')->with('error', 'Profile gagal diubah');
        }
    }
}
