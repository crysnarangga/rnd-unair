<?php

namespace App\Http\Controllers;
use App\Models\UserFoto;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{
    public function index()
    {
        $data['dashboard_menu'] = $this->getDashboardMenu();
        $foto = UserFoto::where('id_user','=', Auth::user()->id)->get();
        $foto_user = $foto[0]->no_dokumen;
        return view('dashboard.dashboard', $data)->with('foto', $foto_user);
    }
}
