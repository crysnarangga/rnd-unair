<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\JenisUser;
use App\Models\UserActivity;
use App\Models\User;
use App\Models\Menu;
use App\Models\MenuUser;
use App\Models\SettingMenu;
use App\Models\UserFoto;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Svg\Tag\Anchor;

class UserController extends Controller{
    
    public function index()
    {
        try{
            $data['dashboard_menu'] = $this->getDashboardMenu();
            $data['user'] = User::get()->where('user.dalete_mark','=','0');
            $foto = UserFoto::where('id_user','=', Auth::user()->id)->get();
            $foto_user = $foto[0]->no_dokumen;
            $data['jenisuser'] = JenisUser::get();
            $data['menu_user'] = MenuUser::get();
            $data['menu'] = Menu::where('delete_mark','=','0')->get();
            UserActivity::create([
                'id_user' => Auth::user()->id,
                'discripsi' => 'Melihat User',
                'status' => 'Success',
                'menu_id' => 'USR',
                'create_by' => Auth::user()->id,
                'delete' => '0'
            ]);
            return view('usermanagement.user1', $data)->with('foto', $foto_user);
        }catch(\Exception $e){
            UserActivity::create([
                'id_user' => Auth::user()->id,
                'discripsi' => 'Melihat User',
                'status' => 'Failed',
                'menu_id' => 'USR',
                'create_by' => Auth::user()->id,
                'delete' => '0'
            ]);
            return response()->json(['error' => $e->getMessage()], 500);
        }
    }

    public function list(Request $request)
    {
        $user = User::leftJoin('user_foto', 'user_foto.id_user', '=', 'user.id')
        ->select ('*','user.id AS user_id')
        ->leftJoin('jenis_user', 'jenis_user.id', '=', 'user.id_jenis_user')
        ->where('user.dalete_mark','=','0')->get();
        //$user =User::all();

        if($request->ajax()){
            return datatables()->of($user)
                        ->addColumn('action', function($data){

                            $button = 
                            '<div class="btn-group dropend">
                            <button type="button" class="btn btn-primary dropdown-toggle" data-bs-toggle="dropdown" aria-expanded="false" data-bs-auto-close="true" aria-haspopup="true">
                             <i class="fa fa-bars"></i>
                            </button>
                            <ul class="dropdown-menu">
                            <li><a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$data->user_id.'" data-original-title="Edit" class="edit edit-post dropdown-item" style="color: #FFA500;"><i class="far fa-edit" style="color: #FFA500;"></i> Edit</a></li>
                            <li><button type="button" name="delete" id="'.$data->user_id.'" class="delete dropdown-item" style="color: red;"><i class="far fa-trash-alt" style="color: red;"></i> Delete</button></li>
                            <li><a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$data->user_id.'" data-original-title="Menu" class=" menu-post dropdown-item" style="color: #21de92;"><i class="far fa-clipboard" style="color: #21de92;"></i> Menu</a></li>
                            <li><a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$data->user_id.'" data-original-title="password" class=" password-post dropdown-item" style="color: #3455eb;"><i class="fa-solid fa-key" style="color: #3455eb;"></i> Change Password</a></li>
                            </ul>
                          </div>';
                          
                            return $button;

                        })

                        ->rawColumns(['action'])
                        ->addIndexColumn()
                        ->make(true);
        }    
    }

    public function check(Request $request)
    {
        if($request->get('username')){
            $username = $request->get('username');
            $data = User::where('username', $username)->count();
            if($data >0){
                echo 'not_unique';
            }else{
                echo 'unique';
            }
        }
    }

    public function store(Request $request)
    {
        try{        
            $post   =   User::create(
                        [
                            'nama_user' => $request->nama_user,
                            'username' => $request->username,
                            'password' => Hash::make('123'),
                            'email' => $request->email,
                            'no_hp' => $request->hp,
                            'wa' => $request->wa,
                            'pin' => $request->pin,
                            'id_jenis_user' => $request->userid,
                            'status_user' => $request->status,
                            'dalete_mark'=> '0',
                            'create_by'=> Auth::user()->id,
                            'update_by'=> Auth::user()->id,
                        ]);
                
            $lastid = $post->id;

            if ($request->file('photo')) {
                $file= $request->file('photo');
                $filename= date('YmdHi').$file->getClientOriginalName();
                $file-> move(public_path('assets/media/img/profile'), $filename);
                $post = UserFoto::create(
                    [
                        'id_user' => $lastid,
                        'no_dokumen' => $filename,
                        'create_by'=> Auth::user()->id,
                        'update_by'=> Auth::user()->id,
                        'dalete_mark'=> '0',
                    ]);
            }else{
                $post = UserFoto::create(
                    [
                        'id_user' => $lastid,
                        'no_dokumen' => 'default.jpg',
                        'create_by'=> Auth::user()->id,
                        'update_by'=> Auth::user()->id,
                        'dalete_mark'=> '0',
                    ]);
            }

            $post = SettingMenu::get()->where('id_jenis_user', '=',$request->userid);
            if(count($post)){
                foreach ($post as $key => $value) {
                    MenuUser::create([
                        'id_user' => $lastid,
                        'menu_id' => $value->menu_id,
                        'create_by'=> Auth::user()->id,
                        'update_by'=> Auth::user()->id,
                        'dalete_mark'=> '0',
                    ]);
                }
            }

            UserActivity::create([
                'id_user' => Auth::user()->id,
                'discripsi' => 'Tambah User',
                'status' => 'Success',
                'menu_id' => 'USR',
                'create_by' => Auth::user()->id,
                'delete' => '0'
            ]);

            return response()->json($post);
        }catch(\Exception $e){
            UserActivity::create([
                'id_user' => Auth::user()->id,
                'discripsi' => 'Tambah User',
                'status' => 'Failed',
                'menu_id' => 'USR',
                'create_by' => Auth::user()->id,
                'delete' => '0'
            ]);
            return response()->json(['error' => $e->getMessage()], 500);
        }
        
        
    }

    public function update(Request $request)
    {
        try{
            $id = $request->id;
        
            $post   =   User::where('id', $id)->update(
                        [
                            'nama_user' => $request->nama_user,
                            'username' => $request->username,
                            'email' => $request->email,
                            'no_hp' => $request->hp,
                            'wa' => $request->wa,
                            'pin' => $request->pin,
                            'id_jenis_user' => $request->userid,
                            'status_user' => $request->status,
                            'update_by'=> Auth::user()->id,
                        ]);
            if ($request->file('photo')) {
                $file= $request->file('photo');
                $filename= date('YmdHi').$file->getClientOriginalName();
                $file-> move(public_path('assets/media/img/profile'), $filename);
                $post = UserFoto::where('id_user', $id)->update(
                    [
                        'no_dokumen' => $filename,
                        'update_by'=> Auth::user()->id,
                    ]);
            }else{
                $foto = UserFoto::where('id_user', '=', $id)->get();
                $post = UserFoto::where('id_user', $id)->update(
                    [
                        'no_dokumen' => $foto->first()->no_dokumen,
                        'update_by'=> Auth::user()->id,
                    ]);
            }

            UserActivity::create([
                'id_user' => Auth::user()->id,
                'discripsi' => 'Update User',
                'status' => 'Success',
                'menu_id' => 'USR',
                'create_by' => Auth::user()->id,
                'delete' => '0'
            ]);
            return response()->json($post);
        }catch(\Exception $e){
            UserActivity::create([
                'id_user' => Auth::user()->id,
                'discripsi' => 'Update User',
                'status' => 'Failed',
                'menu_id' => 'USR',
                'create_by' => Auth::user()->id,
                'delete' => '0'
            ]);
            return response()->json(['error' => $e->getMessage()], 500);
        }
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try{
            $where = array('user.id' => $id);
            $post = User::join('user_foto', 'user_foto.id_user', '=', 'user.id')->select ('*','user.id AS user_id', 'user_foto.id AS user_foto')->where($where)->first();
            // $post  = User::where($where)->first();
            UserActivity::create([
                'id_user' => Auth::user()->id,
                'discripsi' => 'Mengambil Data User',
                'status' => 'Success',
                'menu_id' => 'USR',
                'create_by' => Auth::user()->id,
                'delete' => '0'
            ]);
         
            return response()->json($post);
        }catch(\Exception $e){
            UserActivity::create([
                'id_user' => Auth::user()->id,
                'discripsi' => 'Mengambil Data User',
                'status' => 'Failed',
                'menu_id' => 'USR',
                'create_by' => Auth::user()->id,
                'delete' => '0'
            ]);
            
        }
       
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            $post = User::where('id',$id)->update([
                'dalete_mark' => '1',
                'update_by' => Auth::user()->id,
            ]);
        
            UserActivity::create([
                'id_user' => Auth::user()->id,
                'discripsi' => 'Hapus Data User',
                'status' => 'Success',
                'menu_id' => 'USR',
                'create_by' => Auth::user()->id,
                'delete' => '0'
            ]);
            return response()->json($post);
        }catch(\Exception $e){
            UserActivity::create([
                'id_user' => Auth::user()->id,
                'discripsi' => 'Hapus Data User',
                'status' => 'Failed',
                'menu_id' => 'USR',
                'create_by' => Auth::user()->id,
                'delete' => '0'
            ]);
            return response()->json(['error' => $e->getMessage()], 500);
        }
        
    }

    public function listMenu($id){
        try{
            $post = MenuUser::where('id_user',$id)->get();
            UserActivity::create([
                'id_user' => Auth::user()->id,
                'discripsi' => 'Mengambil Data Menu User',
                'status' => 'Success',
                'menu_id' => 'USR',
                'create_by' => Auth::user()->id,
                'delete' => '0'
            ]);
            return response()->json($post);
        }catch(\Exception $e){
            UserActivity::create([
                'id_user' => Auth::user()->id,
                'discripsi' => 'Mengambil Data Menu User',
                'status' => 'Failed',
                'menu_id' => 'USR',
                'create_by' => Auth::user()->id,
                'delete' => '0'
            ]);
            return response()->json(['error' => $e->getMessage()], 500);
        }
    }

    public function storeMenu(Request $request){
        try{
            if ($request->iduser == null) {
                foreach ($request->menu as $key => $value) {
                    $post = MenuUser::create(
                        [
                            'id_user' => $request->iduser,
                            'menu_id' => $request->menu[$key],
                            'create_by'=> Auth::user()->id,
                            'update_by'=> Auth::user()->id,
                            'dalete_mark'=> '0',
                        ]);
                }
                $parent = [];
                $setting_menu = MenuUser::join('menu','menu.id','=','menu_user.menu_id')
                                    ->where('menu_user.id_user',$request->iduser)
                                    ->where('menu.parent_id','!=', '0')
                                    ->get();
                $menu = Menu::where('parent_id','=', null)->get();
                foreach ($setting_menu as $key => $value) {
                    foreach ($menu as $key => $value) {
                        if ($value->id == $setting_menu[$key]->parent_id) {
                            $parent[] = $value->id;
                        }
                    }
                }
                $parent2 = array_unique($parent);
                foreach ($parent2 as $key => $value) {
                    $post = MenuUser::create(
                        [
                            'id_user' => $request->iduser,
                            'menu_id' => $value,
                            'create_by'=> Auth::user()->id,
                            'update_by'=> Auth::user()->id,
                            'dalete_mark'=> '0',
                        ]);
                }
            } else {
                if ($request->menu == null) {
                    $post = MenuUser::where('id_user',$request->iduser)->delete();
                } else {
                    $post = MenuUser::where('id_user',$request->iduser)->delete();
                    foreach ($request->menu as $key => $value) {
                        $post = MenuUser::create(
                            [
                                'id_user' => $request->iduser,
                                'menu_id' => $request->menu[$key],
                                'create_by'=> Auth::user()->id,
                                'update_by'=> Auth::user()->id,
                                'dalete_mark'=> '0',
                            ]);
                    }
                    $parent = [];
                    
                $setting_menu = MenuUser::join('menu','menu.id','=','menu_user.menu_id')
                                    ->where('menu_user.id_user',$request->iduser)
                                    ->where('menu.parent_id','!=', '0')
                                    ->get();
                $menu = Menu::where('parent_id','=', null)->get();
                foreach ($setting_menu as $key => $value) {
                    foreach ($menu as $key => $value) {
                        if ($value->id == $setting_menu[$key]->parent_id) {
                            $parent[] = $value->id;
                        }
                    }
                }
                $parent2 = array_unique($parent);
                foreach ($parent2 as $key => $value) {
                    $post = MenuUser::create(
                        [
                            'id_user' => $request->iduser,
                            'menu_id' => $value,
                            'create_by'=> Auth::user()->id,
                            'update_by'=> Auth::user()->id,
                            'dalete_mark'=> '0',
                        ]);
                }
                }
            }
            
            UserActivity::create([
                'id_user' => Auth::user()->id,
                'discripsi' => 'Menambah setting menu user',
                'status' => 'Success',
                'menu_id' => 'USR',
                'create_by' => Auth::user()->id,
                'delete' => '0'
            ]);
         
            return response()->json($post);
        }catch(\Exception $e){
            UserActivity::create([
                'id_user' => Auth::user()->id,
                'discripsi' => 'Menambah setting menu user',
                'status' => 'Failed',
                'menu_id' => 'USR',
                'create_by' => Auth::user()->id,
                'delete' => '0'
            ]);
            return response()->json(['error' => $e->getMessage()], 500);
        }
       
    }

    public function changePassword(Request $request){
        try{
            if ($request->password_baru == $request->konfirmasi_password) {
                $post = User::where('id',$request->iduser_password)->update(
                    [
                        'password' => Hash::make($request->password_baru),
                        'update_by'=> Auth::user()->id,
                    ]);
            } else {
                return response()->json(['error' => 'Password baru tidak sama dengan konfirmasi password'], 500);
            }

            UserActivity::create([
                'id_user' => Auth::user()->id,
                'discripsi' => 'Mengubah Password User',
                'status' => 'Success',
                'menu_id' => 'USR',
                'create_by' => Auth::user()->id,
                'delete' => '0'
            ]);
            return response()->json($post);
        }catch(\Exception $e){
            UserActivity::create([
                'id_user' => Auth::user()->id,
                'discripsi' => 'Mengubah Password User',
                'status' => 'Failed',
                'menu_id' => 'USR',
                'create_by' => Auth::user()->id,
                'delete' => '0'
            ]);
            return response()->json(['error' => $e->getMessage()], 500);
        }
    }

}
