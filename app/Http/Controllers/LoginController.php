<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\UserFoto;
use Illuminate\Support\Facades\Auth;
use Validator;


class LoginController extends Controller
{
     public function index()
    {
        if (Auth::check()) {
            return redirect('/default');
        }
        return view('login');
    }

    public function store(Request $request)
    {

        $validated = $request->validate([
            'username' => 'required',
            'password' => 'required',
            // Hide 'g-recaptcha-response' => 'recaptcha',
        ]);

        $credentials                  = $request->all('username', 'password');
        $emailCredentials['username']    = $request['username'];
        $emailCredentials['password'] = $request['password'];
        $user =Auth::user();
        if (Auth::attempt($credentials)) {
            return redirect()->intended('/default');
        } elseif (Auth::attempt($emailCredentials)) {            
            return redirect('/default');
        }else{
            $response=[
                'username' => [trans("messages.username_not_match")],
                'password' => [trans("messages.password_not_match")],
            ];
            return redirect('/login')->withErrors($response);
        }
    }

    public function destroy(Request $request)
    {
        Auth::logout();
        return redirect('/login');   
    }
}
