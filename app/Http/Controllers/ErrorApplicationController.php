<?php

namespace App\Http\Controllers;

use App\Models\ErrorApplication;
use App\Models\UserFoto;
use Error;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ErrorApplicationController extends Controller
{
    public function index()
    {
        $foto = UserFoto::where('id_user','=', Auth::user()->id)->get();
        $foto_user = $foto[0]->no_dokumen;
        return view('usermanagement.ErrorApplication')->with('foto', $foto_user);
    }
    public function list(Request $request)
    {
        $jenis = ErrorApplication::all();
        if($request->ajax()){
            return datatables()->of($jenis)
            ->make(true);
        }     
    }

}
