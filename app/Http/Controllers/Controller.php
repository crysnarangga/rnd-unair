<?php

namespace App\Http\Controllers;

use App\Models\JenisUser;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Models\Role;
use App\Models\Menu;
use App\Models\MenuUser;
use App\Models\User;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function getDashboardMenu()
    {
        $datas['menu']= MenuUser::where('id_user',Auth::user()->id)->get()->pluck('menu_id')->unique();
        $arr = Menu::wherein('id', $datas['menu'])->where('delete_mark','0')->orderBy('menu_name', 'ASC')->get()->toArray();
        return $this->buildTree($arr, 0);    
    }

    private function buildTree(array $elements, $parentId = 0) {
        $branch = array();

        foreach ($elements as $element) {
            if ($element['parent_id'] == $parentId) {
                $children = $this->buildTree($elements, $element['id']);
                if ($children) {
                    $element['menu_childs'] = $children;
                }
                $branch[] = $element+=['menu_childs'=>[]];
            }
        }

        return $branch;
    }
}
