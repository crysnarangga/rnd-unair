<?php

namespace App\Http\Controllers;


use App\Models\UserActivity;
use App\Models\UserFoto;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserActivityController extends Controller
{
    public function index()
    {
        try {
            $data['dashboard_menu'] = $this->getDashboardMenu();
            $foto = UserFoto::where('id_user','=', Auth::user()->id)->get();
            $foto_user = $foto[0]->no_dokumen;
            UserActivity::create([
                'id_user' => Auth::user()->id,
                'discripsi' => 'Melihat User Activity',
                'status' => 'Success',
                'menu_id' => 'ACT',
                'create_by' => Auth::user()->id,
                'delete' => '0'
            ]);
            return view('usermanagement.UserActivity', $data)->with('foto', $foto_user);
        } catch (\Exception $e) {
            UserActivity::create([
                'id_user' => Auth::user()->id,
                'discripsi' => 'Melihat User Activity',
                'status' => 'Failed',
                'menu_id' => 'ACT',
                'create_by' => Auth::user()->id,
                'delete' => '0'
            ]);
            return response()->json(['error' => $e->getMessage()], 500);
        }
    }


    public function list(Request $request)
    {
       
                
        $jenis = UserActivity::leftJoin('user as b', 'b.id', '=', 'user_activity.id_user')
        ->select('user_activity.*', 'b.nama_user as nama_user')
        ->orderBy('no_activity','asc')
                ->get();
        if($request->ajax()){
            return datatables()->of($jenis)
            ->make(true);
        } 
    }

    
}
