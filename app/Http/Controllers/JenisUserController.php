<?php

namespace App\Http\Controllers;


use App\Models\JenisUser;
use App\Models\Menu;
use App\Models\SettingMenu;
use App\Models\UserActivity;
use Illuminate\Http\Request;
use App\Models\UserFoto;
use Illuminate\Support\Facades\Auth;

class JenisUserController extends Controller
{
  
    public function index()
    {
        try {
            $data['dashboard_menu'] = $this->getDashboardMenu();
            $foto = UserFoto::where('id_user','=', Auth::user()->id)->get();
            $foto_user = $foto[0]->no_dokumen;
            $data['jenisuser'] = JenisUser::get();
            UserActivity::create([
                'id_user' => Auth::user()->id,
                'discripsi' => 'Melihat Jenis User',  
                'status' => 'Success',
                'menu_id' => 'JEN',
                'create_by' => Auth::user()->id,
                'delete' => '0'
            ]);
            $data['menu'] = Menu::where('delete_mark', '0')->get();
            return view('usermanagement.jenis-user', $data)->with('foto', $foto_user);
        } catch (\Exception $e) {
            UserActivity::create([
                'id_user' => Auth::user()->id,
                'discripsi' => 'Melihat Jenis User',
                'status' => 'Failed',
                'menu_id' => 'JEN',
                'create_by' => Auth::user()->id,
                'delete' => '0'
            ]);
            return response()->json(['error' => $e->getMessage()], 500);
        }
       

    }

    public function list(Request $request)
    {
        $jenis = JenisUser::all();
        if($request->ajax()){
            return datatables()->of($jenis)
                        ->addColumn('action', function($data){
                            $button = 
                            '<div class="btn-group dropend">
                            <button type="button" class="btn btn-primary dropdown-toggle" data-bs-toggle="dropdown" aria-expanded="false">
                             <i class="fa fa-bars"></i>
                            </button>
                            <ul class="dropdown-menu" style="background-color: white;">
                                    <li><a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$data->id.'" data-original-title="Edit" class="edit edit-post dropdown-item" style="color: #FFA500;"><i class="far fa-edit" style="color: #FFA500;"></i> Edit</a></li>
                                    <li><button type="button" name="delete" id="'.$data->id.'" class="delete dropdown-item" style="color: red;"><i class="far fa-trash-alt" style="color: red;"></i> Delete</button></li>
                                    <li><a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$data->id.'" data-original-title="Menu" class=" menu-post dropdown-item" style="color: #21de92;"><i class="far fa-clipboard" style="color: #21de92;"></i> Menu</a></li>                                </ul>
                            </div>';
                            // $button = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$data->id.'" data-original-title="Edit" class="edit btn btn-info btn-sm edit-post"><i class="far fa-edit"></i> Edit</a>';
                            // $button .= '&nbsp;&nbsp;';
                            // $button .= '<button type="button" name="delete" id="'.$data->id.'" class="delete btn btn-danger btn-sm"><i class="far fa-trash-alt"></i> Delete</button>';     
                            return $button;
                        })
                        ->rawColumns(['action'])
                        ->addIndexColumn()
                        ->make(true);
        }    
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $id = $request->id;
        
            $post   =   JenisUser::updateOrCreate(['id' => $id],
                        [
                            'jenis_user' => $request->jenis_user,
                        ]); 

                        UserActivity::create([
                            'id_user' => Auth::user()->id,
                            'discripsi' => 'Membuat / Mengupdate Jenis User',
                            'status' => 'Success',
                            'menu_id' => 'JEN',
                            'create_by' => Auth::user()->id,
                            'delete' => '0'
                        ]);
            return response()->json($post);
        }catch (\Exception $e) {
            UserActivity::create([
                'id_user' => Auth::user()->id,
                'discripsi' => 'Membuat / Mengupdate Jenis User',
                'status' => 'Failed',
                'menu_id' => 'JEN',
                'create_by' => Auth::user()->id,
                'delete' => '0'
            ]);
            return response()->json(['error' => $e->getMessage()], 500);
        }
        
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            $where = array('id' => $id);
            $post  = JenisUser::where($where)->first();
     
            UserActivity::create([
                'id_user' => Auth::user()->id,
                'discripsi' => 'Melihat Jenis User',
                'status' => 'Success',
                'menu_id' => 'JEN',
                'create_by' => Auth::user()->id,
                'delete' => '0'
            ]);
            return response()->json($post);
        } catch (\Exception $e) {
            UserActivity::create([
                'id_user' => Auth::user()->id,
                'discripsi' => 'Melihat Jenis User',
                'status' => 'Failed',
                'menu_id' => 'JEN',
                'create_by' => Auth::user()->id,
                'delete' => '0'
            ]);
            return response()->json(['error' => $e->getMessage()], 500);
        }
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $post = JenisUser::where('id',$id)->delete();
            UserActivity::create([
                'id_user' => Auth::user()->id,
                'discripsi' => 'Menghapus Jenis User',
                'status' => 'Success',
                'menu_id' => 'JEN',
                'create_by' => Auth::user()->id,
                'delete' => '0'
            ]);
            return response()->json($post);
        }catch(\Exception $e){
            UserActivity::create([
                'id_user' => Auth::user()->id,
                'discripsi' => 'Menghapus Jenis User',
                'status' => 'Failed',
                'menu_id' => 'JEN',
                'create_by' => Auth::user()->id,
                'delete' => '0'
            ]);
            return response()->json(['error' => $e->getMessage()], 500);
        }
        
    }
    //

    public function listMenu($id){
        try{
            $post = SettingMenu::where('id_jenis_user',$id)->get();
            UserActivity::create([
                'id_user' => Auth::user()->id,
                'discripsi' => 'Mengambil Data Menu Jenis User',
                'status' => 'Success',
                'menu_id' => 'USR',
                'create_by' => Auth::user()->id,
                'delete' => '0'
            ]);
            return response()->json($post);
        }catch(\Exception $e){
            UserActivity::create([
                'id_user' => Auth::user()->id,
                'discripsi' => 'Mengambil Data Menu Jenis User',
                'status' => 'Failed',
                'menu_id' => 'USR',
                'create_by' => Auth::user()->id,
                'delete' => '0'
            ]);
            return response()->json(['error' => $e->getMessage()], 500);
        }
    }

    public function storeMenu(Request $request){
        try{
            if ($request->idjenisuser == null) {
                foreach ($request->menu as $key => $value) {
                    $post = SettingMenu::create(
                        [
                            'id_jenis_user' => $request->idjenisuser,
                            'menu_id' => $request->menu[$key],
                            'create_by'=> Auth::user()->id,
                            'update_by'=> Auth::user()->id,
                            'dalete_mark'=> '0',
                        ]);
                }
                $parent = [];
                $setting_menu = SettingMenu::join('menu','menu.id','=','setting_menu_user.menu_id')
                                    ->where('setting_menu_user.id_jenis_user',$request->idjenisuser)
                                    ->where('menu.parent_id','!=', '0')
                                    ->get();
                $menu = Menu::where('parent_id','=', '0')->get();
                foreach ($setting_menu as $key => $value) {
                    foreach ($menu as $key => $value) {
                        if ($value->id == $setting_menu[$key]->parent_id) {
                            $parent[] = $value->id;
                        }
                    }
                }
                $parent2 = array_unique($parent);
                foreach ($parent2 as $key => $value) {
                    $post = SettingMenu::create(
                        [
                            'id_jenis_user' => $request->idjenisuser,
                            'menu_id' => $value,
                            'create_by'=> Auth::user()->id,
                            'update_by'=> Auth::user()->id,
                            'dalete_mark'=> '0',
                        ]);
                }
            } else {
                if ($request->menu == null) {
                    $post = SettingMenu::where('id_jenis_user',$request->idjenisuser)->delete();
                } else {
                    $post = SettingMenu::where('id_jenis_user',$request->idjenisuser)->delete();
                    foreach ($request->menu as $key => $value) {
                        $post = SettingMenu::create(
                            [
                                'id_jenis_user' => $request->idjenisuser,
                                'menu_id' => $request->menu[$key],
                                'create_by'=> Auth::user()->id,
                                'update_by'=> Auth::user()->id,
                                'dalete_mark'=> '0',
                            ]);
                    }
                    $parent = [];
                    $setting_menu = SettingMenu::join('menu','menu.id','=','setting_menu_user.menu_id')
                                        ->where('setting_menu_user.id_jenis_user',$request->idjenisuser)
                                        ->where('menu.parent_id','!=','0')
                                        ->get();
                    $menu = Menu::where('parent_id','=', '0')->get();
                    foreach ($setting_menu as $key => $value) {
                        foreach ($menu as $key => $value) {
                            if ($value->id == $setting_menu[$key]->parent_id) {
                                $parent[] = $value->id;
                            }
                        }
                    }
                    $parent2 = array_unique($parent);
                    foreach ($parent2 as $key => $value) {
                        $post = SettingMenu::create(
                            [
                                'id_jenis_user' => $request->idjenisuser,
                                'menu_id' => $value,
                                'create_by'=> Auth::user()->id,
                                'update_by'=> Auth::user()->id,
                                'dalete_mark'=> '0',
                            ]);
                    }
                }
            }
            
            UserActivity::create([
                'id_user' => Auth::user()->id,
                'discripsi' => 'Menambah setting menu jenis user',
                'status' => 'Success',
                'menu_id' => 'USR',
                'create_by' => Auth::user()->id,
                'delete' => '0'
            ]);
         
            return response()->json($post);
        }catch(\Exception $e){
            UserActivity::create([
                'id_user' => Auth::user()->id,
                'discripsi' => 'Menambah setting menu jenis user',
                'status' => 'Failed',
                'menu_id' => 'USR',
                'create_by' => Auth::user()->id,
                'delete' => '0'
            ]);
            return response()->json(['error' => $e->getMessage()], 500);
        }
       
    }

    

}
