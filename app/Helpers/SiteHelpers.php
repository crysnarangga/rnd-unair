<?php

namespace App\Http\Controllers;

use App\Models\JenisUser ;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public static function main_menu ()
    {
       $main_menu = DB::table('menu_user')->join('menu','menu.id','=','menu_user.menu_id')
       ->select('menu.*', 'menu_user.*')
       ->where('menu_user.id_user', Auth::user()->id)->get()
       ->where('menu.id_level','1');

       return $main_menu; 
    }

    public static function sub_menu ()
    {
       $sub_menu = DB::table('menu_user')->join('menu','menu.id','=','menu_user.menu_id')
       ->select('menu.*', 'menu_user.*')
       ->where('menu_user.id_user', Auth::user()->id)->get()
       ->where('menu.id_level','2');

       return $sub_menu; 
    }

}
