<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMenuUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menu_user', function (Blueprint $table) {
            $table->bigIncrements('no_seting');
            $table->unsignedBigInteger('id_user');
            $table->unsignedBigInteger('menu_id');
            $table->timestamp('create_time');
            $table->string('dalete_mark',1);
            $table->string('update_by',30);
            $table->timestamps();

            $table->foreign('id_user')->references('id')->on('user');
            $table->foreign('menu_id')->references('id')->on('menu');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('menu_user');
    }
}
