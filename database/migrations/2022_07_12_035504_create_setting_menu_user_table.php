<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSettingMenuUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('setting_menu_user', function (Blueprint $table) {
            $table->bigIncrements('no_seting');
            $table->unsignedBigInteger('id_jenis_user');
            $table->unsignedBigInteger('menu_id');
            $table->string('create_by',30);
            $table->string('dalete_mark',1);
            $table->string('update_by',30);
            $table->timestamps();

            $table->foreign('id_jenis_user')->references('id')->on('jenis_user');
            $table->foreign('menu_id')->references('id')->on('menu');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('setting_menu_user');
    }
}
