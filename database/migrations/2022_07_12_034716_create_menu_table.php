<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMenuTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menu', function (Blueprint $table) {
            $table->bigIncrements ('id');
            $table->string('menu_name',300);
            $table->unsignedBigInteger('id_level');
            $table->string('parent_id', 30)->nullable();
            $table->string('menu_link',300);
            $table->string('menu_icon',300);
            $table->string('delete_mark',1);
            $table->string('create_by',30);
            $table->string('update_by',30);
            $table->timestamps();

            $table->foreign('id_level')->references('id')->on('menu_level');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('menu');
    }
}
