<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nama_user',60);
            $table->string('username',60)->unique();
            $table->string('password',60);
            $table->string('email',200);
            $table->string('no_hp',30);
            $table->string('wa',30);
            $table->string('pin',30);
            $table->unsignedBigInteger('id_jenis_user');
            $table->string('status_user',30);
            $table->string('create_by',30);
            $table->string('dalete_mark',1);
            $table->string('update_by',30);
            $table->timestamps();

            $table->foreign('id_jenis_user')->references('id')->on('jenis_user');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user');
    }
}
