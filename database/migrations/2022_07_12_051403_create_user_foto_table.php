<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserFotoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

     

    public function up()
    {
        Schema::create('user_foto', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('id_user');
            $table->string('no_dokumen');
            $table->string('create_by',30);
            $table->string('dalete_mark',1);
            $table->string('update_by',30);
            $table->timestamps();

            $table->foreign('id_user')->references('id')->on('user');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_foto');
    }
}
