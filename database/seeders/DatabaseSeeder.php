<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserSeeder::class);
        $this->call(MenuLevelSeeder::class);
        $this->call(MenuSeeder::class);
        $this->call(SettingMenuUserSeeder::class);
        $this->call(MenuUserSeeder::class);

    }
}
