<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Suport\Str;
use Illuminate\Support\BigIncrements;
class MenuUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('menu_user')->insert([
            "no_seting" => 1,
            "id_user" => "1",
            "menu_id" => "1",
            "create_time" => Carbon::now(),
            "dalete_mark" => "0",
            "update_by" => "1",
            "created_at" => Carbon::now(),
            "updated_at" => Carbon::now()
        ]);
        DB::table('menu_user')->insert([
            "no_seting" => 2,
            "id_user" => "1",
            "menu_id" => "2",
            "create_time" => Carbon::now(),
            "dalete_mark" => "0",
            "update_by" => "1",
            "created_at" => Carbon::now(),
            "updated_at" => Carbon::now()
        ]);
        DB::table('menu_user')->insert([
            "no_seting" => 3,
            "id_user" => "1",
            "menu_id" => "3",
            "create_time" => Carbon::now(),
            "dalete_mark" => "0",
            "update_by" => "1",
            "created_at" => Carbon::now(),
            "updated_at" => Carbon::now()
        ]);
        DB::table('menu_user')->insert([
            "no_seting" => 4,
            "id_user" => "1",
            "menu_id" => "4",
            "create_time" => Carbon::now(),
            "dalete_mark" => "0",
            "update_by" => "1",
            "created_at" => Carbon::now(),
            "updated_at" => Carbon::now()
        ]);
        DB::table('menu_user')->insert([
            "no_seting" => 5,
            "id_user" => "1",
            "menu_id" => "5",
            "create_time" => Carbon::now(),
            "dalete_mark" => "0",
            "update_by" => "1",
            "created_at" => Carbon::now(),
            "updated_at" => Carbon::now()
        ]);
        DB::table('menu_user')->insert([
            "no_seting" => 6,
            "id_user" => "1",
            "menu_id" => "6",
            "create_time" => Carbon::now(),
            "dalete_mark" => "0",
            "update_by" => "1",
            "created_at" => Carbon::now(),
            "updated_at" => Carbon::now()
        ]);
        DB::table('menu_user')->insert([
            "no_seting" => 7,
            "id_user" => "1",
            "menu_id" => "7",
            "create_time" => Carbon::now(),
            "dalete_mark" => "0",
            "update_by" => "1",
            "created_at" => Carbon::now(),
            "updated_at" => Carbon::now()
        ]);
    }
}
