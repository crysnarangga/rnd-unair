<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Suport\Str;
use Illuminate\Support\BigIncrements;


class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('jenis_user')->insert([
            "id" => 1,
            "jenis_user" => "Admin",
            "created_at" => Carbon::now(),
            "updated_at" => Carbon::now()
        ]);

        DB::table('jenis_user')->insert([
            "id" => 2,
            "jenis_user" => "Member",
            "created_at" => Carbon::now(),
            "updated_at" => Carbon::now()
        ]);

        DB::table('user')->insert([
            "id" => 1,
            "nama_user" => "admin1",
            "username"=> "admin11",
            
            "password" => hash::make("admin11"),
            "email" => "admin@gmail.com",
            "no_hp" => "025025123456",
            "wa" => "085789456123",
            "pin" => "123456",
            "id_jenis_user" => "1",
            "status_user" => "1",
            "create_by" => "1",
            "dalete_mark" => "0",
            "update_by" => "1",
            "created_at" => Carbon::now(),
            "updated_at" => Carbon::now()
        ]);

        DB::table('user_foto')->insert([
            "id" => 1,
            "id_user" => "1",
            "no_dokumen" => "default.jpg",
            "create_by" => "1",
            "dalete_mark" => "0",
            "update_by" => "1",
            "created_at" => Carbon::now(),
            "updated_at" => Carbon::now()
        ]);
    }
}
