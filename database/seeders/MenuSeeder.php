<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Suport\Str;
use Illuminate\Support\BigIncrements;


class MenuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('menu')->insert([
            "id" => 1,
            "menu_name" => "Dashboard",
            "id_level" => "1",
            "menu_link" => "default",
            "menu_icon" => "fa-solid fa-house text-primary",
            "delete_mark" => "0",
            "create_by" => "1",
            "update_by" => "1",
            "created_at" => Carbon::now(),
            "updated_at" => Carbon::now()
        ]);
        DB::table('menu')->insert([
            "id" => 2,
            "menu_name" => "User Management",
            "id_level" => "1",
            "menu_link" => "#",
            "menu_icon" => "fa-solid fa-user-gear text-warning",
            "delete_mark" => "0",
            "create_by" => "1",
            "update_by" => "1",
            "created_at" => Carbon::now(),
            "updated_at" => Carbon::now()
        ]);
        DB::table('menu')->insert([
            "id" => 3,
            "menu_name" => "User",
            "id_level" => "2",
            "parent_id" => "2",
            "menu_link" => "user",
            "menu_icon" => "fa-solid fa-user text-warning",
            "delete_mark" => "0",
            "create_by" => "1",
            "update_by" => "1",
            "created_at" => Carbon::now(),
            "updated_at" => Carbon::now()
        ]);
        DB::table('menu')->insert([
            "id" => 4,
            "menu_name" => "Jenis User",
            "id_level" => "2",
            "parent_id" => "2",
            "menu_link" => "jenisuser",
            "menu_icon" => "fa-solid fa-users text-warning",
            "delete_mark" => "0",
            "create_by" => "1",
            "update_by" => "1",
            "created_at" => Carbon::now(),
            "updated_at" => Carbon::now()
        ]);
        DB::table('menu')->insert([
            "id" => 5,
            "menu_name" => "User Activity",
            "id_level" => "2",
            "parent_id" => "2",
            "menu_link" => "useractivity",
            "menu_icon" => "fa-solid fa-person-chalkboard text-warning",
            "delete_mark" => "0",
            "create_by" => "1",
            "update_by" => "1",
            "created_at" => Carbon::now(),
            "updated_at" => Carbon::now()
        ]);
        DB::table('menu')->insert([
            "id" => 6,
            "menu_name" => "Error Application",
            "id_level" => "2",
            "parent_id" => "2",
            "menu_link" => "errorapplication",
            "menu_icon" => "fa-solid fa-person-chalkboard text-warning",
            "delete_mark" => "0",
            "create_by" => "1",
            "update_by" => "1",
            "created_at" => Carbon::now(),
            "updated_at" => Carbon::now()
        ]);
        DB::table('menu')->insert([
            "id" => 7,
            "menu_name" => "Menu",
            "id_level" => "1",
            "menu_link" => "menu",
            "menu_icon" => "fa-solid fa-person-chalkboard text-success",
            "delete_mark" => "0",
            "create_by" => "1",
            "update_by" => "1",
            "created_at" => Carbon::now(),
            "updated_at" => Carbon::now()
        ]);
    }
}
