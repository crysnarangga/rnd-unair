<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Carbon\Carbon;


class MenuLevelSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $menu_level = [
            ['id' => '1', 'level' => 'Parent', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '2', 'level' => 'Child', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
        ];

        \App\Models\MenuLevel::insert($menu_level);
        $this->command->info('Menu Level Seeders SuccessFull');
    }
}
